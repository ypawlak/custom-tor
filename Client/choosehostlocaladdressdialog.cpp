#include "choosehostlocaladdressdialog.h"
#include "ui_choosehostlocaladdressdialog.h"

ChooseHostLocalAddressDialog::ChooseHostLocalAddressDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ChooseHostLocalAddressDialog)
{
    ui->setupUi(this);
}

ChooseHostLocalAddressDialog::~ChooseHostLocalAddressDialog()
{
    delete ui;
}

void ChooseHostLocalAddressDialog::setLocalHostAddress(QHostAddress address)
{
    ui->localHostAddressField->setText(address.toString());
    localHostAddress = address;
}

void ChooseHostLocalAddressDialog::setLocalHostPort(quint16 port)
{
    ui->localHostPortField->setValue(port);
    localHostPort = port;
}

QHostAddress ChooseHostLocalAddressDialog::getLocalHostAddress()
{
    return localHostAddress;
}

quint16 ChooseHostLocalAddressDialog::getLocalHostPort()
{
    return localHostPort;
}

void ChooseHostLocalAddressDialog::on_saveButton_clicked()
{
    bool isAddressCorrect = localHostAddress.setAddress(ui->localHostAddressField->text());

    if(isAddressCorrect) {
      localHostPort = ui->localHostPortField->value();
      accept();
    } else {
        QMessageBox::warning(this, "Invalid address", "Host address you provided could not be parsed correctly.");
    }


}
