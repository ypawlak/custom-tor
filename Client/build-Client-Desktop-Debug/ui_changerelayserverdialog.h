/********************************************************************************
** Form generated from reading UI file 'changerelayserverdialog.ui'
**
** Created by: Qt User Interface Compiler version 5.2.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_CHANGERELAYSERVERDIALOG_H
#define UI_CHANGERELAYSERVERDIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QSpinBox>

QT_BEGIN_NAMESPACE

class Ui_ChangeRELAYServerDialog
{
public:
    QGridLayout *gridLayout;
    QHBoxLayout *horizontalLayout;
    QSpacerItem *horizontalSpacer;
    QLabel *label;
    QSpacerItem *horizontalSpacer_2;
    QHBoxLayout *horizontalLayout_2;
    QLabel *label_2;
    QLineEdit *RELAYAddressField;
    QSpinBox *RELAYPortField;
    QHBoxLayout *horizontalLayout_3;
    QLabel *label_3;
    QLineEdit *hostLocalNameField;
    QPushButton *saveButton;

    void setupUi(QDialog *ChangeRELAYServerDialog)
    {
        if (ChangeRELAYServerDialog->objectName().isEmpty())
            ChangeRELAYServerDialog->setObjectName(QStringLiteral("ChangeRELAYServerDialog"));
        ChangeRELAYServerDialog->resize(400, 143);
        gridLayout = new QGridLayout(ChangeRELAYServerDialog);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);

        label = new QLabel(ChangeRELAYServerDialog);
        label->setObjectName(QStringLiteral("label"));

        horizontalLayout->addWidget(label);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_2);


        gridLayout->addLayout(horizontalLayout, 0, 0, 1, 1);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        label_2 = new QLabel(ChangeRELAYServerDialog);
        label_2->setObjectName(QStringLiteral("label_2"));

        horizontalLayout_2->addWidget(label_2);

        RELAYAddressField = new QLineEdit(ChangeRELAYServerDialog);
        RELAYAddressField->setObjectName(QStringLiteral("RELAYAddressField"));

        horizontalLayout_2->addWidget(RELAYAddressField);

        RELAYPortField = new QSpinBox(ChangeRELAYServerDialog);
        RELAYPortField->setObjectName(QStringLiteral("RELAYPortField"));
        RELAYPortField->setMaximum(65535);

        horizontalLayout_2->addWidget(RELAYPortField);


        gridLayout->addLayout(horizontalLayout_2, 1, 0, 1, 1);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QStringLiteral("horizontalLayout_3"));
        label_3 = new QLabel(ChangeRELAYServerDialog);
        label_3->setObjectName(QStringLiteral("label_3"));

        horizontalLayout_3->addWidget(label_3);

        hostLocalNameField = new QLineEdit(ChangeRELAYServerDialog);
        hostLocalNameField->setObjectName(QStringLiteral("hostLocalNameField"));

        horizontalLayout_3->addWidget(hostLocalNameField);


        gridLayout->addLayout(horizontalLayout_3, 2, 0, 1, 1);

        saveButton = new QPushButton(ChangeRELAYServerDialog);
        saveButton->setObjectName(QStringLiteral("saveButton"));

        gridLayout->addWidget(saveButton, 3, 0, 1, 1);


        retranslateUi(ChangeRELAYServerDialog);

        QMetaObject::connectSlotsByName(ChangeRELAYServerDialog);
    } // setupUi

    void retranslateUi(QDialog *ChangeRELAYServerDialog)
    {
        ChangeRELAYServerDialog->setWindowTitle(QApplication::translate("ChangeRELAYServerDialog", "Dialog", 0));
        label->setText(QApplication::translate("ChangeRELAYServerDialog", "Change RELAY Settings", 0));
        label_2->setText(QApplication::translate("ChangeRELAYServerDialog", "RELAY Address", 0));
        label_3->setText(QApplication::translate("ChangeRELAYServerDialog", "Local name", 0));
        saveButton->setText(QApplication::translate("ChangeRELAYServerDialog", "Save", 0));
    } // retranslateUi

};

namespace Ui {
    class ChangeRELAYServerDialog: public Ui_ChangeRELAYServerDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_CHANGERELAYSERVERDIALOG_H
