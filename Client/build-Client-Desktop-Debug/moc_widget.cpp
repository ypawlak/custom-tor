/****************************************************************************
** Meta object code from reading C++ file 'widget.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.2.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../Client/widget.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'widget.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.2.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_Widget_t {
    QByteArrayData data[16];
    char stringdata[367];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    offsetof(qt_meta_stringdata_Widget_t, stringdata) + ofs \
        - idx * sizeof(QByteArrayData) \
    )
static const qt_meta_stringdata_Widget_t qt_meta_stringdata_Widget = {
    {
QT_MOC_LITERAL(0, 0, 6),
QT_MOC_LITERAL(1, 7, 7),
QT_MOC_LITERAL(2, 15, 0),
QT_MOC_LITERAL(3, 16, 20),
QT_MOC_LITERAL(4, 37, 14),
QT_MOC_LITERAL(5, 52, 8),
QT_MOC_LITERAL(6, 61, 7),
QT_MOC_LITERAL(7, 69, 25),
QT_MOC_LITERAL(8, 95, 39),
QT_MOC_LITERAL(9, 135, 34),
QT_MOC_LITERAL(10, 170, 36),
QT_MOC_LITERAL(11, 207, 32),
QT_MOC_LITERAL(12, 240, 31),
QT_MOC_LITERAL(13, 272, 34),
QT_MOC_LITERAL(14, 307, 22),
QT_MOC_LITERAL(15, 330, 35)
    },
    "Widget\0timeout\0\0readPendingDatagrams\0"
    "processMessage\0Message&\0message\0"
    "on_clearLogButton_clicked\0"
    "on_changeHostLocalAddressButton_clicked\0"
    "on_changeSTUNServersButton_clicked\0"
    "on_changeRELAYSettingsButton_clicked\0"
    "on_testNATPresenceButton_clicked\0"
    "on_sendHEREPacketButton_clicked\0"
    "on_remoteHostNameField_textChanged\0"
    "on_whereButton_clicked\0"
    "on_startCommunicationButton_clicked\0"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Widget[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      12,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,   74,    2, 0x0a,
       3,    0,   75,    2, 0x08,
       4,    1,   76,    2, 0x08,
       7,    0,   79,    2, 0x08,
       8,    0,   80,    2, 0x08,
       9,    0,   81,    2, 0x08,
      10,    0,   82,    2, 0x08,
      11,    0,   83,    2, 0x08,
      12,    0,   84,    2, 0x08,
      13,    0,   85,    2, 0x08,
      14,    0,   86,    2, 0x08,
      15,    0,   87,    2, 0x08,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 5,    6,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void Widget::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Widget *_t = static_cast<Widget *>(_o);
        switch (_id) {
        case 0: _t->timeout(); break;
        case 1: _t->readPendingDatagrams(); break;
        case 2: _t->processMessage((*reinterpret_cast< Message(*)>(_a[1]))); break;
        case 3: _t->on_clearLogButton_clicked(); break;
        case 4: _t->on_changeHostLocalAddressButton_clicked(); break;
        case 5: _t->on_changeSTUNServersButton_clicked(); break;
        case 6: _t->on_changeRELAYSettingsButton_clicked(); break;
        case 7: _t->on_testNATPresenceButton_clicked(); break;
        case 8: _t->on_sendHEREPacketButton_clicked(); break;
        case 9: _t->on_remoteHostNameField_textChanged(); break;
        case 10: _t->on_whereButton_clicked(); break;
        case 11: _t->on_startCommunicationButton_clicked(); break;
        default: ;
        }
    }
}

const QMetaObject Widget::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_Widget.data,
      qt_meta_data_Widget,  qt_static_metacall, 0, 0}
};


const QMetaObject *Widget::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Widget::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_Widget.stringdata))
        return static_cast<void*>(const_cast< Widget*>(this));
    return QWidget::qt_metacast(_clname);
}

int Widget::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 12)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 12;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 12)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 12;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
