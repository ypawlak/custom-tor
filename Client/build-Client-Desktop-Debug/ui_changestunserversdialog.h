/********************************************************************************
** Form generated from reading UI file 'changestunserversdialog.ui'
**
** Created by: Qt User Interface Compiler version 5.2.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_CHANGESTUNSERVERSDIALOG_H
#define UI_CHANGESTUNSERVERSDIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_ChangeSTUNServersDialog
{
public:
    QGridLayout *gridLayout;
    QHBoxLayout *horizontalLayout_3;
    QSpacerItem *horizontalSpacer;
    QLabel *label;
    QSpacerItem *horizontalSpacer_2;
    QHBoxLayout *horizontalLayout;
    QVBoxLayout *verticalLayout;
    QLabel *label_2;
    QLabel *label_3;
    QVBoxLayout *verticalLayout_2;
    QLineEdit *primarySTUNServerAddressEdit;
    QLineEdit *secondarySTUNServerAddressEdit;
    QVBoxLayout *verticalLayout_3;
    QSpinBox *primarySTUNServerPortField;
    QSpinBox *secondarySTUNServerPortField;
    QPushButton *saveButton;

    void setupUi(QDialog *ChangeSTUNServersDialog)
    {
        if (ChangeSTUNServersDialog->objectName().isEmpty())
            ChangeSTUNServersDialog->setObjectName(QStringLiteral("ChangeSTUNServersDialog"));
        ChangeSTUNServersDialog->resize(400, 160);
        gridLayout = new QGridLayout(ChangeSTUNServersDialog);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QStringLiteral("horizontalLayout_3"));
        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer);

        label = new QLabel(ChangeSTUNServersDialog);
        label->setObjectName(QStringLiteral("label"));

        horizontalLayout_3->addWidget(label);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_2);


        gridLayout->addLayout(horizontalLayout_3, 0, 0, 1, 1);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        label_2 = new QLabel(ChangeSTUNServersDialog);
        label_2->setObjectName(QStringLiteral("label_2"));

        verticalLayout->addWidget(label_2);

        label_3 = new QLabel(ChangeSTUNServersDialog);
        label_3->setObjectName(QStringLiteral("label_3"));

        verticalLayout->addWidget(label_3);


        horizontalLayout->addLayout(verticalLayout);

        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        primarySTUNServerAddressEdit = new QLineEdit(ChangeSTUNServersDialog);
        primarySTUNServerAddressEdit->setObjectName(QStringLiteral("primarySTUNServerAddressEdit"));

        verticalLayout_2->addWidget(primarySTUNServerAddressEdit);

        secondarySTUNServerAddressEdit = new QLineEdit(ChangeSTUNServersDialog);
        secondarySTUNServerAddressEdit->setObjectName(QStringLiteral("secondarySTUNServerAddressEdit"));

        verticalLayout_2->addWidget(secondarySTUNServerAddressEdit);


        horizontalLayout->addLayout(verticalLayout_2);

        verticalLayout_3 = new QVBoxLayout();
        verticalLayout_3->setObjectName(QStringLiteral("verticalLayout_3"));
        primarySTUNServerPortField = new QSpinBox(ChangeSTUNServersDialog);
        primarySTUNServerPortField->setObjectName(QStringLiteral("primarySTUNServerPortField"));
        primarySTUNServerPortField->setMaximum(65535);

        verticalLayout_3->addWidget(primarySTUNServerPortField);

        secondarySTUNServerPortField = new QSpinBox(ChangeSTUNServersDialog);
        secondarySTUNServerPortField->setObjectName(QStringLiteral("secondarySTUNServerPortField"));
        secondarySTUNServerPortField->setMaximum(65535);

        verticalLayout_3->addWidget(secondarySTUNServerPortField);


        horizontalLayout->addLayout(verticalLayout_3);


        gridLayout->addLayout(horizontalLayout, 1, 0, 1, 1);

        saveButton = new QPushButton(ChangeSTUNServersDialog);
        saveButton->setObjectName(QStringLiteral("saveButton"));

        gridLayout->addWidget(saveButton, 2, 0, 1, 1);


        retranslateUi(ChangeSTUNServersDialog);

        QMetaObject::connectSlotsByName(ChangeSTUNServersDialog);
    } // setupUi

    void retranslateUi(QDialog *ChangeSTUNServersDialog)
    {
        ChangeSTUNServersDialog->setWindowTitle(QApplication::translate("ChangeSTUNServersDialog", "Dialog", 0));
        label->setText(QApplication::translate("ChangeSTUNServersDialog", "Change STUN Servers", 0));
        label_2->setText(QApplication::translate("ChangeSTUNServersDialog", "Primary", 0));
        label_3->setText(QApplication::translate("ChangeSTUNServersDialog", "Secondary", 0));
        saveButton->setText(QApplication::translate("ChangeSTUNServersDialog", "Save", 0));
    } // retranslateUi

};

namespace Ui {
    class ChangeSTUNServersDialog: public Ui_ChangeSTUNServersDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_CHANGESTUNSERVERSDIALOG_H
