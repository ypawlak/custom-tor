/********************************************************************************
** Form generated from reading UI file 'choosehostlocaladdressdialog.ui'
**
** Created by: Qt User Interface Compiler version 5.2.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_CHOOSEHOSTLOCALADDRESSDIALOG_H
#define UI_CHOOSEHOSTLOCALADDRESSDIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QSpinBox>

QT_BEGIN_NAMESPACE

class Ui_ChooseHostLocalAddressDialog
{
public:
    QGridLayout *gridLayout;
    QLabel *label;
    QHBoxLayout *horizontalLayout;
    QLabel *label_2;
    QLineEdit *localHostAddressField;
    QSpinBox *localHostPortField;
    QHBoxLayout *horizontalLayout_2;
    QSpacerItem *horizontalSpacer;
    QPushButton *saveButton;
    QSpacerItem *horizontalSpacer_2;

    void setupUi(QDialog *ChooseHostLocalAddressDialog)
    {
        if (ChooseHostLocalAddressDialog->objectName().isEmpty())
            ChooseHostLocalAddressDialog->setObjectName(QStringLiteral("ChooseHostLocalAddressDialog"));
        ChooseHostLocalAddressDialog->resize(400, 104);
        gridLayout = new QGridLayout(ChooseHostLocalAddressDialog);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        label = new QLabel(ChooseHostLocalAddressDialog);
        label->setObjectName(QStringLiteral("label"));

        gridLayout->addWidget(label, 0, 0, 1, 1);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        label_2 = new QLabel(ChooseHostLocalAddressDialog);
        label_2->setObjectName(QStringLiteral("label_2"));

        horizontalLayout->addWidget(label_2);

        localHostAddressField = new QLineEdit(ChooseHostLocalAddressDialog);
        localHostAddressField->setObjectName(QStringLiteral("localHostAddressField"));

        horizontalLayout->addWidget(localHostAddressField);

        localHostPortField = new QSpinBox(ChooseHostLocalAddressDialog);
        localHostPortField->setObjectName(QStringLiteral("localHostPortField"));
        localHostPortField->setMaximum(65535);

        horizontalLayout->addWidget(localHostPortField);


        gridLayout->addLayout(horizontalLayout, 1, 0, 1, 1);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer);

        saveButton = new QPushButton(ChooseHostLocalAddressDialog);
        saveButton->setObjectName(QStringLiteral("saveButton"));

        horizontalLayout_2->addWidget(saveButton);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_2);


        gridLayout->addLayout(horizontalLayout_2, 2, 0, 1, 1);


        retranslateUi(ChooseHostLocalAddressDialog);

        QMetaObject::connectSlotsByName(ChooseHostLocalAddressDialog);
    } // setupUi

    void retranslateUi(QDialog *ChooseHostLocalAddressDialog)
    {
        ChooseHostLocalAddressDialog->setWindowTitle(QApplication::translate("ChooseHostLocalAddressDialog", "Dialog", 0));
        label->setText(QApplication::translate("ChooseHostLocalAddressDialog", "<html><head/><body><p align=\"center\"><span style=\" font-size:12pt; font-weight:600;\">Change Host Local Address and Port</span></p></body></html>", 0));
        label_2->setText(QApplication::translate("ChooseHostLocalAddressDialog", "Local address", 0));
        saveButton->setText(QApplication::translate("ChooseHostLocalAddressDialog", "Save", 0));
    } // retranslateUi

};

namespace Ui {
    class ChooseHostLocalAddressDialog: public Ui_ChooseHostLocalAddressDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_CHOOSEHOSTLOCALADDRESSDIALOG_H
