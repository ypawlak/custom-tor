/********************************************************************************
** Form generated from reading UI file 'messageexchangedialog.ui'
**
** Created by: Qt User Interface Compiler version 5.2.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MESSAGEEXCHANGEDIALOG_H
#define UI_MESSAGEEXCHANGEDIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QTextBrowser>

QT_BEGIN_NAMESPACE

class Ui_MessageExchangeDialog
{
public:
    QGridLayout *gridLayout;
    QHBoxLayout *horizontalLayout_2;
    QLabel *label;
    QLabel *remoteAddressLabel;
    QLabel *label_2;
    QSpinBox *offset;
    QPushButton *changeOffsetButton;
    QTextBrowser *logger;
    QHBoxLayout *horizontalLayout;
    QLineEdit *messageField;
    QPushButton *sendButton;
    QPushButton *clearButton;

    void setupUi(QDialog *MessageExchangeDialog)
    {
        if (MessageExchangeDialog->objectName().isEmpty())
            MessageExchangeDialog->setObjectName(QStringLiteral("MessageExchangeDialog"));
        MessageExchangeDialog->resize(444, 300);
        gridLayout = new QGridLayout(MessageExchangeDialog);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        label = new QLabel(MessageExchangeDialog);
        label->setObjectName(QStringLiteral("label"));

        horizontalLayout_2->addWidget(label);

        remoteAddressLabel = new QLabel(MessageExchangeDialog);
        remoteAddressLabel->setObjectName(QStringLiteral("remoteAddressLabel"));

        horizontalLayout_2->addWidget(remoteAddressLabel);

        label_2 = new QLabel(MessageExchangeDialog);
        label_2->setObjectName(QStringLiteral("label_2"));

        horizontalLayout_2->addWidget(label_2);

        offset = new QSpinBox(MessageExchangeDialog);
        offset->setObjectName(QStringLiteral("offset"));
        offset->setMinimum(-65536);
        offset->setMaximum(65536);

        horizontalLayout_2->addWidget(offset);

        changeOffsetButton = new QPushButton(MessageExchangeDialog);
        changeOffsetButton->setObjectName(QStringLiteral("changeOffsetButton"));

        horizontalLayout_2->addWidget(changeOffsetButton);


        gridLayout->addLayout(horizontalLayout_2, 0, 0, 1, 1);

        logger = new QTextBrowser(MessageExchangeDialog);
        logger->setObjectName(QStringLiteral("logger"));

        gridLayout->addWidget(logger, 1, 0, 1, 1);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        messageField = new QLineEdit(MessageExchangeDialog);
        messageField->setObjectName(QStringLiteral("messageField"));

        horizontalLayout->addWidget(messageField);

        sendButton = new QPushButton(MessageExchangeDialog);
        sendButton->setObjectName(QStringLiteral("sendButton"));

        horizontalLayout->addWidget(sendButton);


        gridLayout->addLayout(horizontalLayout, 2, 0, 1, 1);

        clearButton = new QPushButton(MessageExchangeDialog);
        clearButton->setObjectName(QStringLiteral("clearButton"));

        gridLayout->addWidget(clearButton, 3, 0, 1, 1);


        retranslateUi(MessageExchangeDialog);

        QMetaObject::connectSlotsByName(MessageExchangeDialog);
    } // setupUi

    void retranslateUi(QDialog *MessageExchangeDialog)
    {
        MessageExchangeDialog->setWindowTitle(QApplication::translate("MessageExchangeDialog", "Dialog", 0));
        label->setText(QApplication::translate("MessageExchangeDialog", "Remote:", 0));
        remoteAddressLabel->setText(QApplication::translate("MessageExchangeDialog", "192.168.1.10:65535", 0));
        label_2->setText(QApplication::translate("MessageExchangeDialog", "Offset", 0));
        changeOffsetButton->setText(QApplication::translate("MessageExchangeDialog", "Change", 0));
        sendButton->setText(QApplication::translate("MessageExchangeDialog", "Send", 0));
        clearButton->setText(QApplication::translate("MessageExchangeDialog", "Clear", 0));
    } // retranslateUi

};

namespace Ui {
    class MessageExchangeDialog: public Ui_MessageExchangeDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MESSAGEEXCHANGEDIALOG_H
