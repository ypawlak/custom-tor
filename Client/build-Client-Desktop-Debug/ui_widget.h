/********************************************************************************
** Form generated from reading UI file 'widget.ui'
**
** Created by: Qt User Interface Compiler version 5.2.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_WIDGET_H
#define UI_WIDGET_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QTextBrowser>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Widget
{
public:
    QGridLayout *gridLayout_5;
    QGroupBox *groupBox_2;
    QGridLayout *gridLayout_6;
    QHBoxLayout *horizontalLayout;
    QLabel *label_13;
    QLabel *hostLocalAddressLabel;
    QHBoxLayout *horizontalLayout_2;
    QLabel *label_15;
    QLabel *hostAddressFromPrimarySTUNLabel;
    QHBoxLayout *horizontalLayout_3;
    QLabel *label_17;
    QLabel *hostAddressFromSecondarySTUNLabel;
    QHBoxLayout *horizontalLayout_4;
    QLabel *label_19;
    QLabel *hostAddressFromRELAYLabel;
    QPushButton *changeHostLocalAddressButton;
    QGroupBox *groupBox;
    QGridLayout *gridLayout;
    QPushButton *testNATPresenceButton;
    QGridLayout *gridLayout_2;
    QLabel *label;
    QLabel *NATDetectionStatusLabel;
    QLabel *label_2;
    QLabel *NATDetectionLabel;
    QLabel *label_3;
    QLabel *FirewallDetectionLabel;
    QPushButton *clearLogButton;
    QTextBrowser *logBox;
    QGroupBox *groupBox_4;
    QGridLayout *gridLayout_4;
    QHBoxLayout *horizontalLayout_6;
    QLabel *label_9;
    QLabel *RELAYAddressLabel;
    QGridLayout *gridLayout_7;
    QLineEdit *remoteHostNameField;
    QPushButton *whereButton;
    QLabel *remoteHostAddressLabel;
    QPushButton *startCommunicationButton;
    QHBoxLayout *horizontalLayout_7;
    QPushButton *sendHEREPacketButton;
    QPushButton *changeRELAYSettingsButton;
    QHBoxLayout *horizontalLayout_5;
    QLabel *label_22;
    QLabel *hostNameLabel;
    QGroupBox *groupBox_3;
    QGridLayout *gridLayout_3;
    QLabel *secondarySTUNAddressLabel;
    QPushButton *changeSTUNServersButton;
    QLabel *label_8;
    QLabel *primarySTUNAddressLabel;
    QLabel *label_7;

    void setupUi(QWidget *Widget)
    {
        if (Widget->objectName().isEmpty())
            Widget->setObjectName(QStringLiteral("Widget"));
        Widget->resize(559, 753);
        gridLayout_5 = new QGridLayout(Widget);
        gridLayout_5->setSpacing(6);
        gridLayout_5->setContentsMargins(11, 11, 11, 11);
        gridLayout_5->setObjectName(QStringLiteral("gridLayout_5"));
        groupBox_2 = new QGroupBox(Widget);
        groupBox_2->setObjectName(QStringLiteral("groupBox_2"));
        gridLayout_6 = new QGridLayout(groupBox_2);
        gridLayout_6->setSpacing(6);
        gridLayout_6->setContentsMargins(11, 11, 11, 11);
        gridLayout_6->setObjectName(QStringLiteral("gridLayout_6"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(6);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        label_13 = new QLabel(groupBox_2);
        label_13->setObjectName(QStringLiteral("label_13"));

        horizontalLayout->addWidget(label_13);

        hostLocalAddressLabel = new QLabel(groupBox_2);
        hostLocalAddressLabel->setObjectName(QStringLiteral("hostLocalAddressLabel"));

        horizontalLayout->addWidget(hostLocalAddressLabel);


        gridLayout_6->addLayout(horizontalLayout, 0, 0, 1, 1);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        label_15 = new QLabel(groupBox_2);
        label_15->setObjectName(QStringLiteral("label_15"));

        horizontalLayout_2->addWidget(label_15);

        hostAddressFromPrimarySTUNLabel = new QLabel(groupBox_2);
        hostAddressFromPrimarySTUNLabel->setObjectName(QStringLiteral("hostAddressFromPrimarySTUNLabel"));

        horizontalLayout_2->addWidget(hostAddressFromPrimarySTUNLabel);


        gridLayout_6->addLayout(horizontalLayout_2, 1, 0, 1, 1);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setSpacing(6);
        horizontalLayout_3->setObjectName(QStringLiteral("horizontalLayout_3"));
        label_17 = new QLabel(groupBox_2);
        label_17->setObjectName(QStringLiteral("label_17"));

        horizontalLayout_3->addWidget(label_17);

        hostAddressFromSecondarySTUNLabel = new QLabel(groupBox_2);
        hostAddressFromSecondarySTUNLabel->setObjectName(QStringLiteral("hostAddressFromSecondarySTUNLabel"));

        horizontalLayout_3->addWidget(hostAddressFromSecondarySTUNLabel);


        gridLayout_6->addLayout(horizontalLayout_3, 2, 0, 1, 1);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setSpacing(6);
        horizontalLayout_4->setObjectName(QStringLiteral("horizontalLayout_4"));
        label_19 = new QLabel(groupBox_2);
        label_19->setObjectName(QStringLiteral("label_19"));

        horizontalLayout_4->addWidget(label_19);

        hostAddressFromRELAYLabel = new QLabel(groupBox_2);
        hostAddressFromRELAYLabel->setObjectName(QStringLiteral("hostAddressFromRELAYLabel"));

        horizontalLayout_4->addWidget(hostAddressFromRELAYLabel);


        gridLayout_6->addLayout(horizontalLayout_4, 3, 0, 1, 1);

        changeHostLocalAddressButton = new QPushButton(groupBox_2);
        changeHostLocalAddressButton->setObjectName(QStringLiteral("changeHostLocalAddressButton"));

        gridLayout_6->addWidget(changeHostLocalAddressButton, 4, 0, 1, 1);


        gridLayout_5->addWidget(groupBox_2, 0, 0, 1, 1);

        groupBox = new QGroupBox(Widget);
        groupBox->setObjectName(QStringLiteral("groupBox"));
        gridLayout = new QGridLayout(groupBox);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        testNATPresenceButton = new QPushButton(groupBox);
        testNATPresenceButton->setObjectName(QStringLiteral("testNATPresenceButton"));

        gridLayout->addWidget(testNATPresenceButton, 2, 1, 1, 1);

        gridLayout_2 = new QGridLayout();
        gridLayout_2->setSpacing(6);
        gridLayout_2->setObjectName(QStringLiteral("gridLayout_2"));
        label = new QLabel(groupBox);
        label->setObjectName(QStringLiteral("label"));

        gridLayout_2->addWidget(label, 0, 0, 1, 1);

        NATDetectionStatusLabel = new QLabel(groupBox);
        NATDetectionStatusLabel->setObjectName(QStringLiteral("NATDetectionStatusLabel"));

        gridLayout_2->addWidget(NATDetectionStatusLabel, 0, 1, 1, 1);

        label_2 = new QLabel(groupBox);
        label_2->setObjectName(QStringLiteral("label_2"));

        gridLayout_2->addWidget(label_2, 1, 0, 1, 1);

        NATDetectionLabel = new QLabel(groupBox);
        NATDetectionLabel->setObjectName(QStringLiteral("NATDetectionLabel"));

        gridLayout_2->addWidget(NATDetectionLabel, 1, 1, 1, 1);

        label_3 = new QLabel(groupBox);
        label_3->setObjectName(QStringLiteral("label_3"));

        gridLayout_2->addWidget(label_3, 2, 0, 1, 1);

        FirewallDetectionLabel = new QLabel(groupBox);
        FirewallDetectionLabel->setObjectName(QStringLiteral("FirewallDetectionLabel"));

        gridLayout_2->addWidget(FirewallDetectionLabel, 2, 1, 1, 1);


        gridLayout->addLayout(gridLayout_2, 3, 1, 1, 1);


        gridLayout_5->addWidget(groupBox, 3, 0, 1, 1);

        clearLogButton = new QPushButton(Widget);
        clearLogButton->setObjectName(QStringLiteral("clearLogButton"));

        gridLayout_5->addWidget(clearLogButton, 5, 0, 1, 1);

        logBox = new QTextBrowser(Widget);
        logBox->setObjectName(QStringLiteral("logBox"));

        gridLayout_5->addWidget(logBox, 4, 0, 1, 1);

        groupBox_4 = new QGroupBox(Widget);
        groupBox_4->setObjectName(QStringLiteral("groupBox_4"));
        gridLayout_4 = new QGridLayout(groupBox_4);
        gridLayout_4->setSpacing(6);
        gridLayout_4->setContentsMargins(11, 11, 11, 11);
        gridLayout_4->setObjectName(QStringLiteral("gridLayout_4"));
        horizontalLayout_6 = new QHBoxLayout();
        horizontalLayout_6->setSpacing(6);
        horizontalLayout_6->setObjectName(QStringLiteral("horizontalLayout_6"));
        label_9 = new QLabel(groupBox_4);
        label_9->setObjectName(QStringLiteral("label_9"));

        horizontalLayout_6->addWidget(label_9);

        RELAYAddressLabel = new QLabel(groupBox_4);
        RELAYAddressLabel->setObjectName(QStringLiteral("RELAYAddressLabel"));

        horizontalLayout_6->addWidget(RELAYAddressLabel);


        gridLayout_4->addLayout(horizontalLayout_6, 0, 0, 1, 1);

        gridLayout_7 = new QGridLayout();
        gridLayout_7->setSpacing(6);
        gridLayout_7->setObjectName(QStringLiteral("gridLayout_7"));
        remoteHostNameField = new QLineEdit(groupBox_4);
        remoteHostNameField->setObjectName(QStringLiteral("remoteHostNameField"));

        gridLayout_7->addWidget(remoteHostNameField, 0, 0, 1, 1);

        whereButton = new QPushButton(groupBox_4);
        whereButton->setObjectName(QStringLiteral("whereButton"));

        gridLayout_7->addWidget(whereButton, 0, 1, 1, 1);

        remoteHostAddressLabel = new QLabel(groupBox_4);
        remoteHostAddressLabel->setObjectName(QStringLiteral("remoteHostAddressLabel"));

        gridLayout_7->addWidget(remoteHostAddressLabel, 0, 2, 1, 1);

        startCommunicationButton = new QPushButton(groupBox_4);
        startCommunicationButton->setObjectName(QStringLiteral("startCommunicationButton"));

        gridLayout_7->addWidget(startCommunicationButton, 0, 3, 1, 1);


        gridLayout_4->addLayout(gridLayout_7, 3, 0, 1, 1);

        horizontalLayout_7 = new QHBoxLayout();
        horizontalLayout_7->setSpacing(6);
        horizontalLayout_7->setObjectName(QStringLiteral("horizontalLayout_7"));
        sendHEREPacketButton = new QPushButton(groupBox_4);
        sendHEREPacketButton->setObjectName(QStringLiteral("sendHEREPacketButton"));

        horizontalLayout_7->addWidget(sendHEREPacketButton);

        changeRELAYSettingsButton = new QPushButton(groupBox_4);
        changeRELAYSettingsButton->setObjectName(QStringLiteral("changeRELAYSettingsButton"));

        horizontalLayout_7->addWidget(changeRELAYSettingsButton);


        gridLayout_4->addLayout(horizontalLayout_7, 2, 0, 1, 1);

        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setSpacing(6);
        horizontalLayout_5->setObjectName(QStringLiteral("horizontalLayout_5"));
        label_22 = new QLabel(groupBox_4);
        label_22->setObjectName(QStringLiteral("label_22"));

        horizontalLayout_5->addWidget(label_22);

        hostNameLabel = new QLabel(groupBox_4);
        hostNameLabel->setObjectName(QStringLiteral("hostNameLabel"));

        horizontalLayout_5->addWidget(hostNameLabel);


        gridLayout_4->addLayout(horizontalLayout_5, 1, 0, 1, 1);


        gridLayout_5->addWidget(groupBox_4, 2, 0, 1, 1);

        groupBox_3 = new QGroupBox(Widget);
        groupBox_3->setObjectName(QStringLiteral("groupBox_3"));
        gridLayout_3 = new QGridLayout(groupBox_3);
        gridLayout_3->setSpacing(6);
        gridLayout_3->setContentsMargins(11, 11, 11, 11);
        gridLayout_3->setObjectName(QStringLiteral("gridLayout_3"));
        secondarySTUNAddressLabel = new QLabel(groupBox_3);
        secondarySTUNAddressLabel->setObjectName(QStringLiteral("secondarySTUNAddressLabel"));

        gridLayout_3->addWidget(secondarySTUNAddressLabel, 1, 1, 1, 1);

        changeSTUNServersButton = new QPushButton(groupBox_3);
        changeSTUNServersButton->setObjectName(QStringLiteral("changeSTUNServersButton"));

        gridLayout_3->addWidget(changeSTUNServersButton, 3, 0, 1, 2);

        label_8 = new QLabel(groupBox_3);
        label_8->setObjectName(QStringLiteral("label_8"));

        gridLayout_3->addWidget(label_8, 1, 0, 1, 1);

        primarySTUNAddressLabel = new QLabel(groupBox_3);
        primarySTUNAddressLabel->setObjectName(QStringLiteral("primarySTUNAddressLabel"));

        gridLayout_3->addWidget(primarySTUNAddressLabel, 0, 1, 1, 1);

        label_7 = new QLabel(groupBox_3);
        label_7->setObjectName(QStringLiteral("label_7"));

        gridLayout_3->addWidget(label_7, 0, 0, 1, 1);


        gridLayout_5->addWidget(groupBox_3, 1, 0, 1, 1);


        retranslateUi(Widget);

        QMetaObject::connectSlotsByName(Widget);
    } // setupUi

    void retranslateUi(QWidget *Widget)
    {
        Widget->setWindowTitle(QApplication::translate("Widget", "Widget", 0));
        groupBox_2->setTitle(QApplication::translate("Widget", "Local client settings", 0));
        label_13->setText(QApplication::translate("Widget", "Local", 0));
        hostLocalAddressLabel->setText(QApplication::translate("Widget", "<html><head/><body><p><span style=\" color:#ff0004;\">127.0.0.1:1234</span></p></body></html>", 0));
        label_15->setText(QApplication::translate("Widget", "Discovered with primary STUN", 0));
        hostAddressFromPrimarySTUNLabel->setText(QApplication::translate("Widget", "UNKNOWN", 0));
        label_17->setText(QApplication::translate("Widget", "Discovered with secondary STUN", 0));
        hostAddressFromSecondarySTUNLabel->setText(QApplication::translate("Widget", "UNKNOWN", 0));
        label_19->setText(QApplication::translate("Widget", "Discovered with RELAY", 0));
        hostAddressFromRELAYLabel->setText(QApplication::translate("Widget", "UNKNOWN", 0));
        changeHostLocalAddressButton->setText(QApplication::translate("Widget", "Change local address and port", 0));
        groupBox->setTitle(QApplication::translate("Widget", "NAT and Firewall detection", 0));
        testNATPresenceButton->setText(QApplication::translate("Widget", "Test NAT presence", 0));
        label->setText(QApplication::translate("Widget", "<html><head/><body><p><span style=\" font-weight:600;\">Status:</span></p></body></html>", 0));
        NATDetectionStatusLabel->setText(QApplication::translate("Widget", "<html><head/><body><p><span style=\" font-size:12pt; color:#2dba0a;\">READY</span></p></body></html>", 0));
        label_2->setText(QApplication::translate("Widget", "<html><head/><body><p><span style=\" font-weight:600;\">NAT:</span></p></body></html>", 0));
        NATDetectionLabel->setText(QApplication::translate("Widget", "<html><head/><body><p><span style=\" font-size:12pt;\">UNKNOWN</span></p></body></html>", 0));
        label_3->setText(QApplication::translate("Widget", "<html><head/><body><p><span style=\" font-weight:600;\">Firewall:</span></p></body></html>", 0));
        FirewallDetectionLabel->setText(QApplication::translate("Widget", "<html><head/><body><p><span style=\" font-size:12pt;\">UNKNOWN</span></p></body></html>", 0));
        clearLogButton->setText(QApplication::translate("Widget", "Clear log", 0));
        groupBox_4->setTitle(QApplication::translate("Widget", "RELAY Server", 0));
        label_9->setText(QApplication::translate("Widget", "Relay address", 0));
        RELAYAddressLabel->setText(QApplication::translate("Widget", "10.0.0.1:8888", 0));
        whereButton->setText(QApplication::translate("Widget", "WHERE", 0));
        remoteHostAddressLabel->setText(QApplication::translate("Widget", "192.168.1.2:12345", 0));
        startCommunicationButton->setText(QApplication::translate("Widget", "Start communication", 0));
        sendHEREPacketButton->setText(QApplication::translate("Widget", "Send HERE packet", 0));
        changeRELAYSettingsButton->setText(QApplication::translate("Widget", "Change RELAY settings", 0));
        label_22->setText(QApplication::translate("Widget", "Host name", 0));
        hostNameLabel->setText(QApplication::translate("Widget", "ZBIGNIEW", 0));
        groupBox_3->setTitle(QApplication::translate("Widget", "STUN Server", 0));
        secondarySTUNAddressLabel->setText(QApplication::translate("Widget", "255.255.255.255:65535", 0));
        changeSTUNServersButton->setText(QApplication::translate("Widget", "Change STUN servers", 0));
        label_8->setText(QApplication::translate("Widget", "Secondary", 0));
        primarySTUNAddressLabel->setText(QApplication::translate("Widget", "192.168.1.1:1024", 0));
        label_7->setText(QApplication::translate("Widget", "Primary", 0));
    } // retranslateUi

};

namespace Ui {
    class Widget: public Ui_Widget {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_WIDGET_H
