/********************************************************************************
** Form generated from reading UI file 'chooselocaladdressandport.ui'
**
** Created by: Qt User Interface Compiler version 5.2.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_CHOOSELOCALADDRESSANDPORT_H
#define UI_CHOOSELOCALADDRESSANDPORT_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_ChooseLocalAddressAndPort
{
public:
    QWidget *widget;
    QHBoxLayout *horizontalLayout_2;
    QLabel *label_2;
    QLineEdit *lineEdit;
    QLabel *label_3;
    QSpinBox *spinBox;
    QWidget *widget1;
    QHBoxLayout *horizontalLayout_3;
    QSpacerItem *horizontalSpacer_3;
    QPushButton *pushButton;
    QSpacerItem *horizontalSpacer_4;
    QWidget *widget2;
    QHBoxLayout *horizontalLayout;
    QSpacerItem *horizontalSpacer;
    QLabel *label;
    QSpacerItem *horizontalSpacer_2;

    void setupUi(QDialog *ChooseLocalAddressAndPort)
    {
        if (ChooseLocalAddressAndPort->objectName().isEmpty())
            ChooseLocalAddressAndPort->setObjectName(QStringLiteral("ChooseLocalAddressAndPort"));
        ChooseLocalAddressAndPort->resize(400, 106);
        widget = new QWidget(ChooseLocalAddressAndPort);
        widget->setObjectName(QStringLiteral("widget"));
        widget->setGeometry(QRect(8, 40, 381, 29));
        horizontalLayout_2 = new QHBoxLayout(widget);
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        horizontalLayout_2->setContentsMargins(0, 0, 0, 0);
        label_2 = new QLabel(widget);
        label_2->setObjectName(QStringLiteral("label_2"));

        horizontalLayout_2->addWidget(label_2);

        lineEdit = new QLineEdit(widget);
        lineEdit->setObjectName(QStringLiteral("lineEdit"));

        horizontalLayout_2->addWidget(lineEdit);

        label_3 = new QLabel(widget);
        label_3->setObjectName(QStringLiteral("label_3"));

        horizontalLayout_2->addWidget(label_3);

        spinBox = new QSpinBox(widget);
        spinBox->setObjectName(QStringLiteral("spinBox"));
        spinBox->setMaximum(65536);

        horizontalLayout_2->addWidget(spinBox);

        widget1 = new QWidget(ChooseLocalAddressAndPort);
        widget1->setObjectName(QStringLiteral("widget1"));
        widget1->setGeometry(QRect(10, 70, 381, 29));
        horizontalLayout_3 = new QHBoxLayout(widget1);
        horizontalLayout_3->setObjectName(QStringLiteral("horizontalLayout_3"));
        horizontalLayout_3->setContentsMargins(0, 0, 0, 0);
        horizontalSpacer_3 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_3);

        pushButton = new QPushButton(widget1);
        pushButton->setObjectName(QStringLiteral("pushButton"));

        horizontalLayout_3->addWidget(pushButton);

        horizontalSpacer_4 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_4);

        widget2 = new QWidget(ChooseLocalAddressAndPort);
        widget2->setObjectName(QStringLiteral("widget2"));
        widget2->setGeometry(QRect(10, 10, 381, 24));
        horizontalLayout = new QHBoxLayout(widget2);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        horizontalLayout->setContentsMargins(0, 0, 0, 0);
        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);

        label = new QLabel(widget2);
        label->setObjectName(QStringLiteral("label"));

        horizontalLayout->addWidget(label);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_2);


        retranslateUi(ChooseLocalAddressAndPort);

        QMetaObject::connectSlotsByName(ChooseLocalAddressAndPort);
    } // setupUi

    void retranslateUi(QDialog *ChooseLocalAddressAndPort)
    {
        ChooseLocalAddressAndPort->setWindowTitle(QApplication::translate("ChooseLocalAddressAndPort", "Dialog", 0));
        label_2->setText(QApplication::translate("ChooseLocalAddressAndPort", "Address", 0));
        lineEdit->setText(QApplication::translate("ChooseLocalAddressAndPort", "127.0.0.1", 0));
        label_3->setText(QApplication::translate("ChooseLocalAddressAndPort", "Port", 0));
        pushButton->setText(QApplication::translate("ChooseLocalAddressAndPort", "Create socket and bind", 0));
        label->setText(QApplication::translate("ChooseLocalAddressAndPort", "<html><head/><body><p><span style=\" font-size:14pt; font-weight:600;\">Client configuration</span></p></body></html>", 0));
    } // retranslateUi

};

namespace Ui {
    class ChooseLocalAddressAndPort: public Ui_ChooseLocalAddressAndPort {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_CHOOSELOCALADDRESSANDPORT_H
