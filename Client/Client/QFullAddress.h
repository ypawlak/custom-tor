#ifndef QFULLADDRESS_H
#define QFULLADDRESS_H

#include <QtNetwork>
#include "../../STUNServer/socketAddress.h"

class QFullAddress {
public:

    QFullAddress() : assigned(false) {}
    QFullAddress(QHostAddress _address, quint16 _port) : address(_address), port(_port) { }
    QFullAddress(SocketAddress &address) {
        this->address = QHostAddress(QString::fromStdString(address.getIpAddress()));
        this->port = QString::fromStdString(address.getPort()).toInt();
    }

    SocketAddress getSocketAddress() {
        string ipAddr = address.toString().toStdString();
        string portStr = QString::number(port).toStdString();
        return SocketAddress(ipAddr, portStr);
    }

    bool assigned;
    QHostAddress address;
    quint16 port;
};

#endif // QFULLADDRESS_H
