#include "messageexchangedialog.h"
#include "ui_messageexchangedialog.h"

MessageExchangeDialog::MessageExchangeDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::MessageExchangeDialog)
{
    ui->setupUi(this);
}

MessageExchangeDialog::~MessageExchangeDialog()
{
    delete ui;
}

void MessageExchangeDialog::setSocket(QUdpSocket *socket)
{
    udpSocket = socket;
}

void MessageExchangeDialog::setAddress(QFullAddress address, quint16 offset = 0)
{
    myAddress.address = address.address;
    myAddress.port = address.port + offset;
    ui->remoteAddressLabel->setText(myAddress.address.toString() + ":" + QString::number(myAddress.port));
    ui->offset->setValue(0);
}

QFullAddress MessageExchangeDialog::getCurrentAddress()
{
    return myAddress;
}

void MessageExchangeDialog::acceptMessage(Message &m)
{
    log(QString::fromStdString("["+SocketAddressSerializer::Serialize(m.Address) +"] " + m.MessageText));
}

void MessageExchangeDialog::log(QString message) {
    ui->logger->append(message);
}

void MessageExchangeDialog::on_changeOffsetButton_clicked()
{
    myAddress.port += ui->offset->value();
    ui->offset->setValue(0);
    ui->remoteAddressLabel->setText(myAddress.address.toString() + ":" + QString::number(myAddress.port));
}

void MessageExchangeDialog::on_clearButton_clicked()
{
    ui->logger->clear();
}

void MessageExchangeDialog::on_sendButton_clicked()
{
    udpSocket->writeDatagram(QByteArray().append(ui->messageField->text()), myAddress.address, myAddress.port);
    log("[SENT] " + ui->messageField->text());
}
