#ifndef CHANGESTUNSERVERSDIALOG_H
#define CHANGESTUNSERVERSDIALOG_H

#include <QDialog>
#include <QMessageBox>
#include "QFullAddress.h"

namespace Ui {
class ChangeSTUNServersDialog;
}

class ChangeSTUNServersDialog : public QDialog
{
    Q_OBJECT

public:
    explicit ChangeSTUNServersDialog(QWidget *parent = 0);
    ~ChangeSTUNServersDialog();

    void setPrimaryAddress(QFullAddress &address);
    void setSecondaryAddress(QFullAddress &address);

    QFullAddress getPrimaryAddress();
    QFullAddress getSecondaryAddress();

private slots:
    void on_saveButton_clicked();

private:
    Ui::ChangeSTUNServersDialog *ui;

    QFullAddress primaryAddress;
    QFullAddress secondaryAddress;
};

#endif // CHANGESTUNSERVERSDIALOG_H
