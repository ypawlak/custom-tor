#ifndef CHANGERELAYSERVERDIALOG_H
#define CHANGERELAYSERVERDIALOG_H

#include <QDialog>
#include "QFullAddress.h"
#include <QMessageBox>

namespace Ui {
class ChangeRELAYServerDialog;
}

class ChangeRELAYServerDialog : public QDialog
{
    Q_OBJECT

public:
    explicit ChangeRELAYServerDialog(QWidget *parent = 0);
    ~ChangeRELAYServerDialog();

    void setRELAYAddress(QFullAddress &address);
    void setHostLocalName(QString &name);
    QFullAddress getRELAYAddress();
    QString getHostLocalName();

private slots:
    void on_saveButton_clicked();

private:
    Ui::ChangeRELAYServerDialog *ui;
    QFullAddress RELAYAddress;
    QString hostLocalName;
};

#endif // CHANGERELAYSERVERDIALOG_H
