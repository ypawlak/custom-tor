#include "widget.h"
#include "ui_widget.h"

Widget::Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget)
{
    socket = nullptr;
    ui->setupUi(this);
    hostLocalAddress.address = QHostAddress::AnyIPv4;
    connect(&timer, SIGNAL(timeout()), this, SLOT(timeout()));
    state = NATRecognizingState::INITIAL;
    updateGUI();
}

Widget::~Widget()
{
    delete ui;
}

void Widget::timeout()
{
    if(state == NATRecognizingState::WAITING_FOR_TEST_ONE) {
        state = NATRecognizingState::UDP_BLOCKED;
    } else if(state == NATRecognizingState::NAT_PRESENT_WAITING_FOR_TEST_TWO) {
        Message m = STUNProtocol::createSTUNMessage(secondarySTUNServerAddress.getSocketAddress(), false, false);
        send(m);
        timer.start(TIMEOUT);
        state = NATRecognizingState::WAITING_FOR_TEST_ONE_DUAL_SERVER;
    } else if(state == NATRecognizingState::WAITING_FOR_TEST_ONE_DUAL_SERVER) {
        state = NATRecognizingState::SECOND_STUN_SERVER_UNREACHABLE;
    } else if(state == NATRecognizingState::NO_NAT_WAITING_FOR_TEST_TWO) {
        state = NATRecognizingState::SYMMETRIC_FIREWALL;
    } else if(state == NATRecognizingState::WAITING_FOR_TEST_THREE) {
        state = NATRecognizingState::RESTRICTED_PORT_NAT;
    }

    updateGUI();
}

void Widget::createServer() {
    if(socket != nullptr) delete socket;
    socket = new QUdpSocket(this);
    bool result = socket->bind(hostLocalAddress.address, hostLocalAddress.port);
    if(result == false) {
        log("[LOCAL] Creation unsuccessful. Reason: " + socket->errorString());
        hostLocalAddress.assigned = false;
    } else {
        log("[LOCAL] Creation successful.");
        hostLocalAddress.assigned = true;
        hostLocalAddress.address = socket->localAddress();
        hostLocalAddress.port = socket->localPort();
    }
    connect(socket, SIGNAL(readyRead()),
            this, SLOT(readPendingDatagrams()));
}

void Widget::readPendingDatagrams()
{
    while (socket->hasPendingDatagrams()) {
        QByteArray datagram;
        datagram.resize(socket->pendingDatagramSize());
        QHostAddress sender;
        quint16 senderPort;

        socket->readDatagram(datagram.data(), datagram.size(),
                                &sender, &senderPort);
        log("[" + sender.toString() + ":" + QString("%1").arg(senderPort) + "] " + QString(datagram));

        Message message(SocketAddress(sender.toString().toStdString(), QString("%1").arg(senderPort).toStdString()), QString(datagram).toStdString());
        processMessage(message);
    }
}

void Widget::processMessage(Message &m) {
    if(messageDialog != nullptr) {
        QFullAddress remote = messageDialog->getCurrentAddress();
        QFullAddress local(m.Address);
        if(remote.address == local.address && remote.port == local.port) {
            messageDialog->acceptMessage(m);
        }
    }
    try {
        SocketAddress nutsResponse = STUNProtocol::readNUTSMessage(m);
        QFullAddress source(m.Address);
        if(source.address == primarySTUNServerAddress.address && source.port == primarySTUNServerAddress.port) {
            hostDiscoveredFromPrimarySTUNServerAddress = QFullAddress(nutsResponse);
            hostDiscoveredFromPrimarySTUNServerAddress.assigned = true;
            updateGUI();
        } else if(source.address == secondarySTUNServerAddress.address && source.port == secondarySTUNServerAddress.port) {
            hostDiscoveredFromSecondarySTUNServerAddress = QFullAddress(nutsResponse);
            hostDiscoveredFromSecondarySTUNServerAddress.assigned = true;
            updateGUI();
        }
        processNUTSMessage(nutsResponse, m.Address);
    } catch(string s) {
        try{
            RELAYPacketType type = RELAYProtocol::checkType(m);
            if(type == RELAYPacketType::THERE) {
                RELAYEntity data = RELAYProtocol::readTHEREMessage(m);
                if(data.Name == hostName.toStdString()) {
                    hostDiscoveredFromRELAYServerAddress = QFullAddress(data.Address);
                    hostDiscoveredFromRELAYServerAddress.assigned = true;
                    updateGUI();
                }
                if(data.Name == remoteHostName.toStdString()) {
                    remoteHostAddress = QFullAddress(data.Address);
                    remoteHostAddress.assigned = true;
                    updateGUI();
                }
            } else if(type == RELAYPacketType::UNKNOWN) {
                string name = RELAYProtocol::readUNKNOWNMessage(m);
                if(name == remoteHostName.toStdString()) {
                    remoteHostAddress.assigned = false;
                    updateGUI();
                }
            }
        } catch(string s) {
            log(QString::fromStdString(s));
        }
    }
}

void Widget::send(Message &message) {
    QFullAddress address(message.Address);
    send(address, QString::fromStdString(message.MessageText));
}

void Widget::send(QFullAddress address, QString message) {
    socket->writeDatagram(QByteArray().append(message), address.address, address.port);
    log("[SENT] message [TO] [" + address.address.toString() + ":" + QString("%1").arg(address.port)+"]");
}

void Widget::log(QString data) {
    ui->logBox->append(data);
}

void Widget::on_clearLogButton_clicked()
{
    ui->logBox->clear();
}

void Widget::on_changeHostLocalAddressButton_clicked()
{
    ChooseHostLocalAddressDialog dialog;
    if(hostLocalAddress.assigned) {
        dialog.setLocalHostAddress(hostLocalAddress.address);
        dialog.setLocalHostPort(hostLocalAddress.port);
    }
    dialog.exec();
    hostLocalAddress.address = dialog.getLocalHostAddress();
    hostLocalAddress.port = dialog.getLocalHostPort();
    createServer();
    updateGUI();
}

void Widget::updateLabel(QLabel* label, QFullAddress &address, QString unassignedMessage, QString unassignedColor) {
    if(address.assigned) {
        label->setStyleSheet("QLabel { color : black; }");
        label->setText(address.address.toString() + ":" + QString::number(address.port));
    } else {
        label->setStyleSheet("QLabel { color : " + unassignedColor + "; }");
        label->setText(unassignedMessage);
    }
}

void Widget::updateLabelWithUnknown(QLabel* label, QFullAddress &address) {
    updateLabel(label, address, "UNKNOWN", "black");
}

void Widget::updateLabelWithUnassigned(QLabel* label, QFullAddress &address) {
    updateLabel(label, address, "UNASSIGNED", "red");
}


void Widget::updateLabels() {
    updateLabelWithUnassigned(ui->hostLocalAddressLabel, hostLocalAddress);
    updateLabelWithUnassigned(ui->primarySTUNAddressLabel, primarySTUNServerAddress);
    updateLabelWithUnassigned(ui->secondarySTUNAddressLabel, secondarySTUNServerAddress);
    updateLabelWithUnassigned(ui->RELAYAddressLabel, relayServerAddress);

    updateLabelWithUnknown(ui->hostAddressFromPrimarySTUNLabel, hostDiscoveredFromPrimarySTUNServerAddress);
    updateLabelWithUnknown(ui->hostAddressFromSecondarySTUNLabel, hostDiscoveredFromSecondarySTUNServerAddress);
    updateLabelWithUnknown(ui->hostAddressFromRELAYLabel, hostDiscoveredFromRELAYServerAddress);
    updateLabelWithUnknown(ui->remoteHostAddressLabel, remoteHostAddress);

    if(hostName.size() > 0) {
        ui->hostNameLabel->setStyleSheet("QLabel { color : black; }");
        ui->hostNameLabel->setText(hostName);
    } else {
        ui->hostNameLabel->setStyleSheet("QLabel { color : red; }");
        ui->hostNameLabel->setText("UNASSIGNED");
    }

    QString statusText = ui->NATDetectionStatusLabel->text();
    QString natText = ui->NATDetectionLabel->text();
    QString firewallText = ui->FirewallDetectionLabel->text();

    switch(state) {
        case NATRecognizingState::INITIAL:
            statusText = "READY";
            natText = firewallText = "UNKNOWN";
            break;
        case NATRecognizingState::WAITING_FOR_TEST_ONE:
            statusText = "WAITING - TEST #1 PRIMARY";
            natText = firewallText = "UNKNOWN";
            break;
        case NATRecognizingState::WAITING_FOR_TEST_ONE_DUAL_SERVER:
            statusText = "WAITING - TEST #1 SECONDARY";
            natText = firewallText = "UNKNOWN";
            break;
        case NATRecognizingState::UDP_BLOCKED:
            statusText = "UDP_BLOCKED";
            natText = firewallText = "UNKNOWN";
            break;
        case NATRecognizingState::NAT_PRESENT_WAITING_FOR_TEST_TWO:
            statusText = "WAITING - TEST #2 PRIMARY";
            natText = "PRESENT";
            firewallText = "UNKNOWN";
            break;
        case NATRecognizingState::NO_NAT_WAITING_FOR_TEST_TWO:
            statusText = "WAITING - TEST #2 PRIMARY";
            natText = "NOT DETECTED";
            firewallText = "UNKNOWN";
            break;
        case NATRecognizingState::SYMMETRIC_FIREWALL:
            statusText = "DONE";
            natText = "NOT DETECTED";
            firewallText = "SYMMETRIC";
            break;
        case NATRecognizingState::OPEN_INTERNET:
            statusText = "OPEN INTERNET";
            natText = firewallText = "NOT DETECTED";
            break;
        case NATRecognizingState::FULL_CONE_NAT:
            statusText = "DONE";
            natText = "FULL-CONE";
            firewallText = "UNKNOWN";
            break;
        case NATRecognizingState::WAITING_FOR_TEST_THREE:
            statusText = "WAITING - TEST #3 SECONDARY";
            natText = "DETECTED";
            firewallText = "UNKNOWN";
        case NATRecognizingState::SYMMETRIC_NAT:
            statusText = "DONE";
            natText = "SYMMETRIC";
            firewallText = "UNKNOWN";
            break;
        case NATRecognizingState::RESTRICTED_CONE_NAT:
            statusText = "DONE";
            natText = "RESTRICTED-CONE";
            firewallText = "UNKNOWN";
            break;
        case NATRecognizingState::RESTRICTED_PORT_NAT:
            statusText = "DONE";
            natText = "RESTRICTED-PORT";
            firewallText = "UNKNOWN";
            break;
        case NATRecognizingState::SECOND_STUN_SERVER_UNREACHABLE:
            statusText = "SECONDARY-STUN SERVER UNREACHABLE";
            natText = "UNKNOWN";
            firewallText = "UNKNOWN";
            break;
    }

    ui->NATDetectionStatusLabel->setText(statusText);
    ui->NATDetectionLabel->setText(natText);
    ui->FirewallDetectionLabel->setText(firewallText);
}

void Widget::updateButtons()
{
    if(hostLocalAddress.assigned == false ||
            hostName.size() == 0 ||
            relayServerAddress.assigned == false) {
        ui->sendHEREPacketButton->setEnabled(false);
    } else {
        ui->sendHEREPacketButton->setEnabled(true);
    }

    if(hostLocalAddress.assigned == false ||
            primarySTUNServerAddress.assigned == false ||
            secondarySTUNServerAddress.assigned == false) {
        ui->testNATPresenceButton->setEnabled(false);
        // albo jest TEST przeprowadzany / przeprowadzony!
    } else {
        ui->testNATPresenceButton->setEnabled(true);
        // wat
    }

    if(ui->remoteHostNameField->text().size() > 0 && hostLocalAddress.assigned == true && relayServerAddress.assigned == true) {
        ui->whereButton->setEnabled(true);
    } else {
        ui->whereButton->setEnabled(false);
    }

    if(remoteHostAddress.assigned && hostLocalAddress.assigned) {
        ui->startCommunicationButton->setEnabled(true);
    } else {
        ui->startCommunicationButton->setEnabled(false);
    }
}

void Widget::updateGUI()
{
    updateLabels();
    updateButtons();
}

void Widget::processNUTSMessage(SocketAddress response, SocketAddress source)
{
    QFullAddress mine(response), servers(source);
    if(state == NATRecognizingState::WAITING_FOR_TEST_ONE) {
        if(servers.address == primarySTUNServerAddress.address && servers.port == primarySTUNServerAddress.port) {
            timer.stop();
            if(hostLocalAddress.address == mine.address && hostLocalAddress.port == mine.port) {
                state = NATRecognizingState::NO_NAT_WAITING_FOR_TEST_TWO;
            } else {
                state = NATRecognizingState::NAT_PRESENT_WAITING_FOR_TEST_TWO;
            }
            Message m = STUNProtocol::createSTUNMessage(secondarySTUNServerAddress.getSocketAddress(), true, true);
            send(m);
            timer.start(TIMEOUT);
        }
    } else if(state == NATRecognizingState::NAT_PRESENT_WAITING_FOR_TEST_TWO) {
        if(servers.address == secondarySTUNServerAddress.address && servers.port == secondarySTUNServerAddress.port) {
            timer.stop();
            state = NATRecognizingState::FULL_CONE_NAT;

        }
    } else if(state == NATRecognizingState::NO_NAT_WAITING_FOR_TEST_TWO) {
        if(servers.address == secondarySTUNServerAddress.address && servers.port == secondarySTUNServerAddress.port) {
            timer.stop();
            state = NATRecognizingState::OPEN_INTERNET;
        }
    } else if(state == NATRecognizingState::WAITING_FOR_TEST_ONE_DUAL_SERVER) {
        if(servers.address == secondarySTUNServerAddress.address && servers.port == secondarySTUNServerAddress.port) {
            timer.stop();
            if(hostDiscoveredFromPrimarySTUNServerAddress.address == mine.address && hostDiscoveredFromPrimarySTUNServerAddress.port == mine.port) {
                state = NATRecognizingState::WAITING_FOR_TEST_THREE;
                Message m = STUNProtocol::createSTUNMessage(secondarySTUNServerAddress.getSocketAddress(), true, false);
                send(m);
                timer.start(TIMEOUT);
            } else {
                state = NATRecognizingState::SYMMETRIC_NAT;
            }
        }
    } else if(state == NATRecognizingState::WAITING_FOR_TEST_THREE) {
        if(servers.address == secondarySTUNServerAddress.address && servers.port == primarySTUNServerAddress.port) {
            timer.stop();
            state = NATRecognizingState::RESTRICTED_CONE_NAT;
        }
    }

    updateGUI();
}

void Widget::on_changeSTUNServersButton_clicked()
{
    ChangeSTUNServersDialog dialog;

    if(primarySTUNServerAddress.assigned) {
        dialog.setPrimaryAddress(primarySTUNServerAddress);
    }

    if(secondarySTUNServerAddress.assigned) {
        dialog.setSecondaryAddress(secondarySTUNServerAddress);
    }

    dialog.exec();

    primarySTUNServerAddress = dialog.getPrimaryAddress();
    secondarySTUNServerAddress = dialog.getSecondaryAddress();

    primarySTUNServerAddress.assigned = true;
    secondarySTUNServerAddress.assigned = true;

    hostDiscoveredFromPrimarySTUNServerAddress.assigned = false;
    hostDiscoveredFromSecondarySTUNServerAddress.assigned = false;

    updateGUI();
}

void Widget::on_changeRELAYSettingsButton_clicked()
{
    ChangeRELAYServerDialog dialog;

    if(relayServerAddress.assigned) {
        dialog.setRELAYAddress(relayServerAddress);
    }

    dialog.setHostLocalName(hostName);

    dialog.exec();

    hostName = dialog.getHostLocalName();

    relayServerAddress = dialog.getRELAYAddress();
    relayServerAddress.assigned = true;

    hostDiscoveredFromRELAYServerAddress.assigned = false;

    updateGUI();
}

void Widget::on_testNATPresenceButton_clicked()
{
    state = NATRecognizingState::WAITING_FOR_TEST_ONE;
    Message message = STUNProtocol::createSTUNMessage(primarySTUNServerAddress.getSocketAddress());
    send(message);
    updateGUI();
    timer.start(TIMEOUT);
}

void Widget::on_sendHEREPacketButton_clicked()
{
    Message message = RELAYProtocol::createHEREMessage(relayServerAddress.getSocketAddress(), hostName.toStdString());
    send(message);
    message = RELAYProtocol::createWHEREMessage(relayServerAddress.getSocketAddress(), hostName.toStdString());
    send(message);
}

void Widget::on_remoteHostNameField_textChanged()
{
    updateGUI();
}

void Widget::on_whereButton_clicked()
{
    remoteHostName = ui->remoteHostNameField->text();
    remoteHostAddress.assigned = false;
    Message message = RELAYProtocol::createWHEREMessage(relayServerAddress.getSocketAddress(), remoteHostName.toStdString());
    send(message);
    updateGUI();
}

void Widget::on_startCommunicationButton_clicked()
{
    messageDialog = new MessageExchangeDialog();
    messageDialog->setAddress(remoteHostAddress, 0);
    messageDialog->setSocket(socket);
    messageDialog->exec();
    delete messageDialog;
    messageDialog = nullptr;
}
