#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QtWidgets>
#include <QtNetwork>

#include "../../STUNServer/utilities.h"
#include "../../STUNServer/clientPacketListener.h"
#include "QFullAddress.h"
#include "../choosehostlocaladdressdialog.h"
#include "changestunserversdialog.h"
#include "changerelayserverdialog.h"
#include "messageexchangedialog.h"

#include "../../STUNServer/STUN.h"
#include "../../STUNServer/RELAY.h"

namespace Ui {
class Widget;
}

enum class NATRecognizingState {INITIAL, WAITING_FOR_TEST_ONE,
                                UDP_BLOCKED, NO_NAT_WAITING_FOR_TEST_TWO,
                                NAT_PRESENT_WAITING_FOR_TEST_TWO, SYMMETRIC_FIREWALL,
                                OPEN_INTERNET, FULL_CONE_NAT, WAITING_FOR_TEST_ONE_DUAL_SERVER,
                                SYMMETRIC_NAT, WAITING_FOR_TEST_THREE, RESTRICTED_CONE_NAT, RESTRICTED_PORT_NAT,
                               SECOND_STUN_SERVER_UNREACHABLE };

class Widget : public QWidget
{
    Q_OBJECT

public:
    explicit Widget(QWidget *parent = 0);
    void createServer();

    void sendHelloToServer();
    void sendHelloToPeer();
    void sendTellToServer();

    void send(Message &message);
    void send(QFullAddress address, QString message);

    ~Widget();

public slots:

    void timeout();

private slots:

    void readPendingDatagrams();
    void processMessage(Message &message);

    void on_clearLogButton_clicked();

    void on_changeHostLocalAddressButton_clicked();

    void on_changeSTUNServersButton_clicked();

    void on_changeRELAYSettingsButton_clicked();

    void on_testNATPresenceButton_clicked();

    void on_sendHEREPacketButton_clicked();

    void on_remoteHostNameField_textChanged();

    void on_whereButton_clicked();

    void on_startCommunicationButton_clicked();

private:
    static const int TIMEOUT = 2000;
    NATRecognizingState state;

    QTimer timer;

    Ui::Widget *ui;
    QUdpSocket *socket;
    MessageExchangeDialog *messageDialog = nullptr;

    QString hostName;
    QString remoteHostName;

    QFullAddress hostLocalAddress;
    QFullAddress remoteHostAddress;

    QFullAddress hostDiscoveredFromPrimarySTUNServerAddress;
    QFullAddress hostDiscoveredFromSecondarySTUNServerAddress;
    QFullAddress hostDiscoveredFromRELAYServerAddress;

    QFullAddress primarySTUNServerAddress;
    QFullAddress secondarySTUNServerAddress;
    QFullAddress relayServerAddress;

    void updateLabel(QLabel* label, QFullAddress &address, QString unassignedMessage, QString unassignedColor);
    void updateLabelWithUnknown(QLabel* label, QFullAddress &address);
    void updateLabelWithUnassigned(QLabel* label, QFullAddress &address);

    map<string, SocketAddress> lookup;

    void log(QString text);
    void updateLabels();
    void updateButtons();
    void updateGUI();
    void processNUTSMessage(SocketAddress response, SocketAddress source);

};

#endif // WIDGET_H
