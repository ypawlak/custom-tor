#ifndef MESSAGEEXCHANGEDIALOG_H
#define MESSAGEEXCHANGEDIALOG_H

#include <QDialog>
#include <QtNetwork>
#include "QFullAddress.h"
#include "../../STUNServer/message.h"

namespace Ui {
class MessageExchangeDialog;
}

class MessageExchangeDialog : public QDialog
{
    Q_OBJECT

public:
    explicit MessageExchangeDialog(QWidget *parent = 0);
    ~MessageExchangeDialog();

    void setSocket(QUdpSocket *socket);
    void setAddress(QFullAddress address, quint16 offset);

    QFullAddress getCurrentAddress();
    void acceptMessage(Message &m);

private slots:
    void on_changeOffsetButton_clicked();

    void on_clearButton_clicked();

    void on_sendButton_clicked();

private:
    Ui::MessageExchangeDialog *ui;
    QUdpSocket *udpSocket;
    QFullAddress myAddress;

    void log(QString message);
};

#endif // MESSAGEEXCHANGEDIALOG_H
