#include "changestunserversdialog.h"
#include "ui_changestunserversdialog.h"

ChangeSTUNServersDialog::ChangeSTUNServersDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ChangeSTUNServersDialog)
{
    ui->setupUi(this);
}

ChangeSTUNServersDialog::~ChangeSTUNServersDialog()
{
    delete ui;
}

void ChangeSTUNServersDialog::setPrimaryAddress(QFullAddress &address)
{
    primaryAddress = address;
    if(address.assigned) {
        ui->primarySTUNServerAddressEdit->setText(primaryAddress.address.toString());
        ui->primarySTUNServerPortField->setValue(primaryAddress.port);
    }
}

void ChangeSTUNServersDialog::setSecondaryAddress(QFullAddress &address)
{
    secondaryAddress = address;
    ui->secondarySTUNServerAddressEdit->setText(secondaryAddress.address.toString());
    ui->secondarySTUNServerPortField->setValue(secondaryAddress.port);
}

QFullAddress ChangeSTUNServersDialog::getPrimaryAddress()
{
    return primaryAddress;
}

QFullAddress ChangeSTUNServersDialog::getSecondaryAddress()
{
    return secondaryAddress;
}

void ChangeSTUNServersDialog::on_saveButton_clicked()
{
    bool isPrimaryAddressCorrect = primaryAddress.address.setAddress(ui->primarySTUNServerAddressEdit->text());
    bool isSecondaryAddressCorrect = secondaryAddress.address.setAddress(ui->secondarySTUNServerAddressEdit->text());

    if(isPrimaryAddressCorrect && isSecondaryAddressCorrect) {
      primaryAddress.port = ui->primarySTUNServerPortField->value();
      secondaryAddress.port = ui->secondarySTUNServerPortField->value();
      accept();
    } else {
        QMessageBox::warning(this, "Invalid address", "Server address you provided could not be parsed correctly.");
    }
}
