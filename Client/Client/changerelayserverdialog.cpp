#include "changerelayserverdialog.h"
#include "ui_changerelayserverdialog.h"

ChangeRELAYServerDialog::ChangeRELAYServerDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ChangeRELAYServerDialog)
{
    ui->setupUi(this);
}

ChangeRELAYServerDialog::~ChangeRELAYServerDialog()
{
    delete ui;
}

void ChangeRELAYServerDialog::setRELAYAddress(QFullAddress &address)
{
    RELAYAddress = address;
    if(address.assigned) {
        ui->RELAYAddressField->setText(address.address.toString());
        ui->RELAYPortField->setValue(address.port);
    }
}

void ChangeRELAYServerDialog::setHostLocalName(QString &name)
{
    hostLocalName = name;
    ui->hostLocalNameField->setText(name);
}

QFullAddress ChangeRELAYServerDialog::getRELAYAddress()
{
    return RELAYAddress;
}

QString ChangeRELAYServerDialog::getHostLocalName()
{
    return hostLocalName;
}

void ChangeRELAYServerDialog::on_saveButton_clicked()
{
    bool isAddressCorrect = RELAYAddress.address.setAddress(ui->RELAYAddressField->text());

    if(isAddressCorrect) {
        RELAYAddress.port = ui->RELAYPortField->value();
        RELAYAddress.assigned = true;
        hostLocalName = ui->hostLocalNameField->text();
        accept();
    } else {
        QMessageBox::warning(this, "Invalid address", "Server address you provided could not be parsed correctly.");
    }
}
