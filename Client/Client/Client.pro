#-------------------------------------------------
#
# Project created by QtCreator 2016-02-25T22:59:02
#
#-------------------------------------------------

QT       += core gui
QT += network
CONFIG += c++11

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Client
TEMPLATE = app

SOURCES += main.cpp\
        widget.cpp\
        ../../STUNServer/hello.cpp\
        ../../STUNServer/socketAddress.cpp\
        ../../STUNServer/tell.cpp\
        ../../STUNServer/utilities.cpp\
        ../../STUNServer/call.cpp\
        ../../STUNServer/called.cpp \
    ../choosehostlocaladdressdialog.cpp \
    changestunserversdialog.cpp \
    changerelayserverdialog.cpp \
    ../../STUNServer/RELAYProtocol.cpp \
    ../../STUNServer/STUNProtocol.cpp \
    messageexchangedialog.cpp

HEADERS  += widget.h\
        ../STUNServer/tell.h \
    ../choosehostlocaladdressdialog.h \
    QFullAddress.h \
    changestunserversdialog.h \
    changerelayserverdialog.h \
    messageexchangedialog.h

FORMS    += widget.ui \
    ../choosehostlocaladdressdialog.ui \
    changestunserversdialog.ui \
    changerelayserverdialog.ui \
    messageexchangedialog.ui
