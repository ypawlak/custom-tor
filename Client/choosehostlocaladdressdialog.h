#ifndef CHOOSEHOSTLOCALADDRESSDIALOG_H
#define CHOOSEHOSTLOCALADDRESSDIALOG_H

#include <QDialog>
#include <QtNetwork>
#include <QMessageBox>

namespace Ui {
class ChooseHostLocalAddressDialog;
}

class ChooseHostLocalAddressDialog : public QDialog
{
    Q_OBJECT

public:
    explicit ChooseHostLocalAddressDialog(QWidget *parent = 0);
    ~ChooseHostLocalAddressDialog();

    void setLocalHostAddress(QHostAddress);
    void setLocalHostPort(quint16);

    QHostAddress getLocalHostAddress();
    quint16 getLocalHostPort();

private slots:
    void on_saveButton_clicked();

private:
    Ui::ChooseHostLocalAddressDialog *ui;

    QHostAddress localHostAddress;
    quint16 localHostPort;
};

#endif // CHOOSEHOSTLOCALADDRESSDIALOG_H
