#include "utilities.h"

bool areCharTablesEqual(string a, string b, int size) {
	for(int i = 0; i < size; i++) {
		if(a[i] != b[i]) return false;
	}
	return true;
}

MessageType checkMessageType(Message m) {
	if(areCharTablesEqual(m.MessageText, "HELLO:", 6)) {
		return MessageType::HELLO;
	} else if(areCharTablesEqual(m.MessageText, "TELL:", 5)) {
		return MessageType::TELL;
	} else if(areCharTablesEqual(m.MessageText, "CALL:", 5)) {
		return MessageType::CALL;
	} else if(areCharTablesEqual(m.MessageText, "TOLD:", 5)) {
		return MessageType::TOLD;
	} else if(areCharTablesEqual(m.MessageText, "UNKNOWN:", 8)) {
		return MessageType::UNKNOWN;
	} else if(areCharTablesEqual(m.MessageText, "CALLED:", 7)) {
		return MessageType::CALLED;
	} else {
		return MessageType::MALFORMED;
	}
}
