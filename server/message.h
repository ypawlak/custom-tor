#pragma once

#include "socketAddress.h"

class Message {
      public:
	Message(SocketAddress address, string messageText) : Address(address), MessageText(messageText) {}
	SocketAddress Address;
	string MessageText;
};
