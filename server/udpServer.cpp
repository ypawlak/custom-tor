#include "udpServer.h"

UdpServer::UdpServer(unsigned short port, int _bufferSize, string address) {
    struct sockaddr_in myAddr;
    
	socketfd = socket(AF_INET, SOCK_DGRAM, 0);
	
	if(socketfd == -1) {
			throw string("Error creating socket\n");
	}
	
	cout << "Socket created\n";
 
    myAddr.sin_family = AF_INET;
    myAddr.sin_port = htons(port);
    myAddr.sin_addr.s_addr = 0;
    
    int errorCode = bind(socketfd, (const sockaddr*) &myAddr, sizeof(myAddr));
    
    if (errorCode < 0) {
        throw string("Error binding socket to port\n");
    }
    
    if(address != "") {
		const char* device = address.c_str();
		if (setsockopt(socketfd, SOL_SOCKET, SO_BINDTODEVICE, (void *) device, address.size()) < 0) {
			close(socketfd);
			throw string("Error binding socket to interface\n");
		}
	}
	
	/* int enable = 1;
	if (setsockopt(socketfd, SOL_SOCKET, SO_REUSEADDR, &enable, sizeof(int)) < 0) {
		close(socketfd);
		throw string("Error setting socket reusable");
	}*/
    
    cout << "Socket bound\n";
    
    bufferSize = _bufferSize;
    buffer = new char[bufferSize];
}

UdpServer::~UdpServer() {
	close(socketfd);
	delete[] buffer;
}

void UdpServer::setListener(UdpPacketListener* _listener) {
	listener = _listener;
}

void UdpServer::send(Message message) {
	int toSend = message.MessageText.size();
	sockaddr_in address = message.Destination.getSocketAddrStruct();
	while(toSend > 0) {
		int sent = sendto(socketfd, message.MessageText.c_str()+(message.MessageText.size())-toSend, toSend, 0, (const sockaddr*) &address, sizeof(sockaddr));
		if(sent < 0) {
			throw string("Error sending data!");
		} else {
			toSend -= sent;
		}
	}
}

void UdpServer::receive() {
	sockaddr_in *sourceAddr = new sockaddr_in;
    unsigned int addrLen = sizeof(sourceAddr);
    
    cout << "Receiving packet...\n";
    
	int totalRead = recvfrom(socketfd,
                             buffer,
                             bufferSize,
                             0,
                             (sockaddr*)sourceAddr,
                             &addrLen);
                             
    if (totalRead < 0) {
        throw string("Error receiving data. Code");
    }
    
    cout << "Packet received\n";
                             
    if(listener != nullptr) {
		Message *message = new Message(SocketAddress(*sourceAddr), SocketAddress(), string(buffer, totalRead));
		listener->handlePacket(*message);
	}
}
