#include "udpServer.h"
#include <thread>

UdpServer server(ANY_PORT, 1000);
Relay *r;
struct sockaddr_in destAddr;

void receive() {
	cout << "START!" << endl;
	while(1) {
		try {
			cout << "RECEIVING...\n";
			server.receive();
		} catch(string s) {
			cout << "Error: " << s << endl;
		}
	}
}

int main(int argc, char* argv[]) {
	
	string name = (argc > 1) ? argv[1] : "MIESNYJEZ";
	unsigned short port = (argc > 2) ? (unsigned short)stoi(argv[2]) : 12345;
	string serverLocation = (argc > 3) ? string(argv[3]) : LOCALHOST;
	
	r = new Relay(&server, name);
	destAddr.sin_family = AF_INET;
	destAddr.sin_port = htons(port);
	destAddr.sin_addr.s_addr = 0;
	inet_aton(serverLocation.c_str(), &destAddr.sin_addr);
	server.setListener(r);
	
	thread t(receive);
	
	while(1) {
		try {
			r->sendHello(&destAddr);
			this_thread::sleep_for(chrono::seconds(5));
			if(argc > 4) {
				r->sendTell(&destAddr, string(argv[4]));
				this_thread::sleep_for(chrono::seconds(5));
			}
		} catch(string s) {
			cout << "Error: " << s << endl;
		}
	}
	return 0;
}
