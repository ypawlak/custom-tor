#pragma once

#include "utilities.h"
#include "message.h"

#define CALL_PREFIX "CALL:"

class Call {
    public:
	  Call() {}
      Call(Message msg);
      SocketAddress Address;
      SocketAddress CallAddress;
      string TargetName;
      Message createMessage();
};


