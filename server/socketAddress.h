#pragma once

#include <iostream>
#include <fcntl.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <cstdio>
#include <errno.h>
#include <cstring>
#include <poll.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>
#include <errno.h>
#include <error.h>
#include <netdb.h>
#include <map>
#include <cstdlib>

using namespace std;

class SocketAddress {
    private:
      struct sockaddr_in socket;
    public:
    SocketAddress() {}
	SocketAddress(string ipAddrStr, string portStr);
	SocketAddress(struct sockaddr_in socket);
	struct sockaddr_in getSocketAddrStruct();
	string getIpAddress();
	string getPort();
};

class SocketAddressSerializer {
    public:
      static string Serialize(SocketAddress toSerialize);
      static SocketAddress Deserialize(string serializedAddress);
};
