#include "STUN.h"
#include <sys/epoll.h>

STUNServerBundle::STUNServerBundle(STUNConfiguration &configuration) {
  SocketAddress primaryPortSecondaryAddress(configuration.SecondaryAddress.getIpAddress(), configuration.PrimaryAddress.getPort());
  SocketAddress secondaryPortPrimaryAddress(configuration.PrimaryAddress.getIpAddress(), configuration.SecondaryAddress.getPort());
  
  primaryAddressPrimaryPortSocket = new STUNSocket(configuration.PrimaryAddress);
  primaryAddressSecondaryPortSocket = new STUNSocket(secondaryPortPrimaryAddress);
  secondaryAddressPrimaryPortSocket = new STUNSocket(primaryPortSecondaryAddress);
  secondaryAddressSecondaryPortSocket = new STUNSocket(configuration.SecondaryAddress);
  
  primaryAddressPrimaryPortSocket->setNeighbourhood(secondaryAddressPrimaryPortSocket, primaryAddressSecondaryPortSocket, secondaryAddressSecondaryPortSocket);
  primaryAddressSecondaryPortSocket->setNeighbourhood(secondaryAddressSecondaryPortSocket, primaryAddressPrimaryPortSocket, secondaryAddressPrimaryPortSocket);
  secondaryAddressPrimaryPortSocket->setNeighbourhood(primaryAddressPrimaryPortSocket, secondaryAddressSecondaryPortSocket, primaryAddressSecondaryPortSocket);
  secondaryAddressSecondaryPortSocket->setNeighbourhood(primaryAddressSecondaryPortSocket, secondaryAddressPrimaryPortSocket, primaryAddressPrimaryPortSocket);
  
  cout << "[START] STUN Server running at addresses " 
	<< configuration.PrimaryAddress.getIpAddress()
	<< " and " << configuration.SecondaryAddress.getIpAddress()
	<< ", ports [" << configuration.PrimaryAddress.getPort()
	<< ", " << configuration.SecondaryAddress.getPort() << "]" << endl;
}

STUNServerBundle::~STUNServerBundle() {
  delete primaryAddressPrimaryPortSocket;
  delete primaryAddressSecondaryPortSocket;
  delete secondaryAddressPrimaryPortSocket;
  delete secondaryAddressSecondaryPortSocket;
}

void STUNServerBundle::start() {
  
  int pp = primaryAddressPrimaryPortSocket->getSocketFD();
  int ps = primaryAddressSecondaryPortSocket->getSocketFD();
  int sp = secondaryAddressPrimaryPortSocket->getSocketFD();
  int ss = secondaryAddressSecondaryPortSocket->getSocketFD();
  
  struct epoll_event eventPP, eventPS, eventSP, eventSS;
  struct epoll_event *events;
  
  int efd = epoll_create1 (0);
  
  if (efd < 0) {
    throw string("Error: cannot create epoll instance!\n");
  }

  eventPP.data.fd = pp;
  eventPS.data.fd = ps;
  eventSP.data.fd = sp;
  eventSS.data.fd = ss;
  
  eventPP.events = eventPS.events = eventSP.events = eventSS.events = EPOLLIN;
  
  if (epoll_ctl (efd, EPOLL_CTL_ADD, pp, &eventPP) < 0) {
    throw string("Error: unable to add socket descriptor to epoll!\n");
  }
  
  if (epoll_ctl (efd, EPOLL_CTL_ADD, ps, &eventPS) < 0) {
    throw string("Error: unable to add socket descriptor to epoll!\n");
  }
  
  if (epoll_ctl (efd, EPOLL_CTL_ADD, sp, &eventSP) < 0) {
    throw string("Error: unable to add socket descriptor to epoll!\n");
  }
  
  if (epoll_ctl (efd, EPOLL_CTL_ADD, ss, &eventSS) < 0) {
    throw string("Error: unable to add socket descriptor to epoll!\n");
  }

  events = (epoll_event*) calloc (MAX_EVENTS, sizeof(eventPP));
  
  cout << "[STUN] STUN Server waiting for incoming transmission. " << endl;
  
  while(true) {
    int eventsNumber = epoll_wait(efd, events, MAX_EVENTS, -1);
    if (eventsNumber < 0) {
      close(efd);
      throw string("Error: exiting epoll_wait returned negative value!\n");
    }

    for (int n = 0; n < eventsNumber; ++n) {
      try {
	if (events[n].data.fd == pp) {
	  primaryAddressPrimaryPortSocket->receive();
	  cout << "[STUN] PrimaryAddress at PrimaryPort received a STUN message." << endl;
	} else if(events[n].data.fd == ps) {
	  primaryAddressSecondaryPortSocket->receive();
	  cout << "[STUN] PrimaryAddress at SecondaryPort received a STUN message." << endl;
	} else if(events[n].data.fd == sp) {
	  secondaryAddressPrimaryPortSocket->receive();
	  cout << "[STUN] SecondaryAddress at PrimaryPort received a STUN message." << endl;
	} else if(events[n].data.fd == ss) {
	  secondaryAddressSecondaryPortSocket->receive();
	  cout << "[STUN] SecondaryAddress at SecondaryPort received a STUN message." << endl;
	} 
      } catch(string s) {
	if(errno != 0) perror(("[STUN][ERROR] " + s).c_str());
	else cout << "[STUN][ERROR] " << s;
      }
    }
  }

  free (events);
  close(efd);
}

void STUNServerBundle::startAndCatchExceptions() {
  try {
    start();
  } catch(string s) {
    cout << "[CRASH] STUN server crashed!" << endl;
    if(errno != 0) perror(("[ERROR] " + s).c_str());
  } catch(...) {
    cout << "[CRASH] STUN server crashed for unknown reason (...) block" << endl;
  }
}