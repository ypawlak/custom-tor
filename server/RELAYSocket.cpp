#include "RELAY.h"

RELAYSocket::RELAYSocket(SocketAddress &address) {
    
  socketFD = socket(AF_INET, SOCK_DGRAM, 0);

  if(socketFD == -1) {
    throw string("Error creating socket\n");
  }
  
  struct sockaddr_in addr = address.getSocketAddrStruct();
    
  int errorCode = bind(socketFD, (const sockaddr*) &(addr), sizeof(addr));
    
  if (errorCode < 0) {
    throw string("Error binding socket to " + address.getIpAddress() + ":" + address.getPort() + "\n");
  }
  
  buffer = new char[bufferSize];
  
  cout << "[START] RELAY server running at " << SocketAddressSerializer::Serialize(address) << endl;
}

RELAYSocket::~RELAYSocket() {
  close(socketFD);
  delete buffer;
}

void RELAYSocket::sendMessage(Message &message) {
  int toSend = message.MessageText.size();
  sockaddr_in address = message.Address.getSocketAddrStruct();
  while(toSend > 0) {
    int sent = sendto(socketFD, message.MessageText.c_str()+(message.MessageText.size())-toSend, toSend, 0, (const sockaddr*) &address, sizeof(sockaddr));
    if(sent < 0) {
      throw string("Error sending data!\n");
    } else {
      toSend -= sent;
    }
  }
}
    
void RELAYSocket::receive() {
  sockaddr_in sourceAddr;
  unsigned int addrLen = sizeof(sourceAddr);
  
  int totalRead = recvfrom(socketFD,
      buffer,
      bufferSize,
      0,
      (sockaddr*) &sourceAddr,
      &addrLen);
  
  if (totalRead < 0) {
    throw string("Error in receive function\n");
  }
  
  Message message(SocketAddress(sourceAddr), string(buffer, totalRead));
  
  string name;
  
  try {
    switch(RELAYProtocol::checkType(message)) {
      case RELAYPacketType::HERE:
	name = RELAYProtocol::readHEREMessage(message);
	lookup[name] = message.Address;
	cout << "[RECV] HERE message from " << SocketAddressSerializer::Serialize(message.Address) << endl;
	cout << "[SENT] ACK message to " << SocketAddressSerializer::Serialize(message.Address) << endl;
	// TODO SEND ACK
	break;
      case RELAYPacketType::WHERE:
	name = RELAYProtocol::readWHEREMessage(message);
	cout << "[RECV] WHERE message from " << SocketAddressSerializer::Serialize(message.Address) << endl;
	if(lookup.find(name) != lookup.end()) {
	  Message thereMessage = RELAYProtocol::createTHEREMessage(message.Address, RELAYEntity(lookup[name], name));
	  sendMessage(thereMessage);
	  cout << "[SENT] THERE message to " << SocketAddressSerializer::Serialize(message.Address) << endl;
	} else {
	  cout << "[SENT] UNKNOWN message to " << SocketAddressSerializer::Serialize(message.Address) << endl;
	  Message unknownMessage = RELAYProtocol::createUNKNOWNMessage(message.Address, name);
	  sendMessage(unknownMessage);
	}
	break;
      default:
	throw string("\n");
    }
  } catch(string error) {
    cout << "[RECV] MALFORMED message from " << SocketAddressSerializer::Serialize(message.Address) << " | " << error;
  }
}

void RELAYSocket::startAndCatchExceptions() {
  try {
    while(true) {
      receive();
    }
  } catch(string s) {
    cout << "[CRASH] RELAY server crashed!" << endl;
    if(errno != 0) perror(("[ERROR] " + s).c_str());
  } catch(...) {
    cout << "[CRASH] RELAY server crashed for unknown reason (...) block" << endl;
  }
}