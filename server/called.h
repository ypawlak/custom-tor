#pragma once

#include "utilities.h"
#include "message.h"

#define CALLED_PREFIX "CALLED:"

class Called {
    public:
	  Called() {}
      Called(Message msg);
      SocketAddress Address;
      SocketAddress CallerAddress;
      SocketAddress CallAddress;
      string OriginName;
      string TargetName;
      Message createMessage();
};
