#pragma once

#include <iostream>
#include <fcntl.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <cstdio>
#include <errno.h>
#include <cstring>
#include <poll.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>
#include <errno.h>
#include <error.h>
#include <netdb.h>
#include <map>
#include <net/if.h>

#include "hello.h"
#include "tell.h"
#include "call.h"
#include "called.h"
#include "udpServer.h"

#define ANY_PORT 0
#define LOCALHOST "127.0.0.1"

using namespace std;

enum class MessageType { HELLO, TELL, TOLD, CALL, CALLED, ACK, MALFORMED, UNKNOWN };

bool areCharTablesEqual(char* a, char* b, int size);

MessageType checkMessageType(Message m);
