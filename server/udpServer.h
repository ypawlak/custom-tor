#pragma once

#include "utilities.h"

class UdpPacketListener {
	public:
		virtual void handlePacket(Message message) = 0;
};

class UdpServer {
	private:
		int socketfd;
		int bufferSize;
		char *buffer;
		UdpPacketListener* listener;
	public:
		UdpServer(unsigned short port, int bufferSize, string address = "");
		~UdpServer();
		void receive();
		void send(Message message);
		void setListener(UdpPacketListener *_listener);
};
