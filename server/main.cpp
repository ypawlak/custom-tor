#include <iostream>
#include <thread>
#include "STUN.h"
#include "RELAY.h"
#include "ServerConfiguration.h"

thread *stunThread = nullptr, *relayThread = nullptr; 
STUNServerBundle *stunServerBundle = nullptr;
RELAYSocket *relaySocket = nullptr;

void setupSTUNServer(ServerConfiguration &conf);
void setupRELAYServer(ServerConfiguration &conf);

int main(int argc, char **argv) {
  
  if(argc == 1 || argv[1] == "-h" || argv[1] == "--help") {
    cout << "USAGE: server [-c configurationFile] [-p primaryAddress:primaryPort]\n[-s secondaryAddress:secondaryPort] [-r relayAddress:relayPort]\n[--stunonly | --relayonly]" << endl;
    exit(0);
  }
  
  try {
    ServerConfiguration conf = CMDArguments::createConfigurationUsingCMDArguments(argc, argv);
    
    if(!conf.relayOnlySet) {
      setupSTUNServer(conf);
    }
    
    if(!conf.stunOnlySet) {
      setupRELAYServer(conf);
    }
    
  } catch(string s) {
    if(errno != 0) perror(s.c_str());
    else cout << s; 
  }
  
  
  if(stunThread != nullptr) {
    stunThread->join();
    delete stunThread;
  }
  
  if(relayThread != nullptr) {
    relayThread->join();
    delete relayThread;
  }
  
  if(stunServerBundle != nullptr) delete stunServerBundle;
  if(relaySocket != nullptr) delete relaySocket;
  
  return 0;
}

void setupSTUNServer(ServerConfiguration &conf) {
  try {
    STUNConfiguration stunConfiguration;

    stunConfiguration.PrimaryAddress = SocketAddressSerializer::Deserialize(conf.primaryAddress);
    stunConfiguration.SecondaryAddress = SocketAddressSerializer::Deserialize(conf.secondaryAddress);
    
    stunServerBundle = new STUNServerBundle(stunConfiguration);
    
    cout << "[THREAD] Attemting to start STUN server in new thread" << endl;
    
    stunThread = new thread(bind(&STUNServerBundle::startAndCatchExceptions, stunServerBundle));
    
  } catch(string s) {
    cout << "[CRASH] STUN server crashed!" << endl;
    if(errno != 0) perror(("[STUN][ERROR] " + s).c_str());
    else cout << "[STUN][ERROR] " << s;
  }
}

void setupRELAYServer(ServerConfiguration &conf) {
  try {
    SocketAddress address = SocketAddressSerializer::Deserialize(conf.relayAddress);
    relaySocket = new RELAYSocket(address);
    
    cout << "[THREAD] Attemting to start RELAY server in new thread" << endl;
    
    relayThread = new thread(bind(&RELAYSocket::startAndCatchExceptions, relaySocket));
  } catch(string s) {
    cout << "[CRASH] RELAY server crashed!" << endl;
    if(errno != 0) perror(("[RELAY][ERROR] " + s).c_str());
    else cout << "[RELAY][ERROR] " << s;
  }
}
