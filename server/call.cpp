#include "call.h"

Call::Call(Message msg) {
    this->Address = SocketAddress(msg.Address.getSocketAddrStruct());
    int atPos = msg.MessageText.find('@');
    int start = string(CALL_PREFIX).length();
    this->TargetName = msg.MessageText.substr(start, atPos -start);
    this->CallAddress = SocketAddressSerializer::Deserialize(msg.MessageText.substr(atPos + 1));
}

Message Call::createMessage() {
    string txt = CALL_PREFIX + this->TargetName + "@" + SocketAddressSerializer::Serialize(this->CallAddress);
    return Message(this->Address, txt);
}
