#pragma once

#include "message.h"
#include "socketAddress.h"

static const string STUN_PREFIX = "STUN";
static const string NUTS_PREFIX = "NUTS";
static const string PORT_PREFIX = "PORT: ";
static const string ADDRESS_PREFIX = "ADDRESS: ";
static const string CHANGE_PORT_PREFIX = "CHANGE PORT: ";
static const string CHANGE_ADDRESS_PREFIX = "CHANGE ADDRESS: ";

class STUNPacket {
  public:
    STUNPacket(SocketAddress _address, bool _changePort, bool _changeAddress) : Address(_address), ChangePort(_changePort), ChangeAddress(_changeAddress) { }
    SocketAddress Address;
    bool ChangePort;
    bool ChangeAddress;
};

class STUNProtocol {
  private:
  public:
    static Message createNUTSMessage(SocketAddress address);
    static Message createSTUNMessage(SocketAddress address, bool changePort = false, bool changeAddress = false);
    static STUNPacket readSTUNMessage(Message message);
    static SocketAddress readNUTSMessage(Message message);
};

class STUNConfiguration {
  public:
    SocketAddress PrimaryAddress;
    SocketAddress SecondaryAddress;
};

class STUNSocket {
  private:
    static const int bufferSize = 1024;
    
    int socketFD;
    char *buffer;
    
    STUNSocket *samePortDifferentAddressSocket;
    STUNSocket *differentPortSameAddressSocket;
    STUNSocket *differentPortDifferentAddressSocket;
    
    void sendMessage(Message &message);
    void handleSTUNPacket(STUNPacket &packet);
    
  public:
    STUNSocket(SocketAddress &address);
    virtual ~STUNSocket();
    
    void setNeighbourhood(STUNSocket *_samePortDifferentAddressSocket, STUNSocket *_differentPortSameAddressSocket, STUNSocket *_differentPortDifferentAddressSocket);
    
    void receive();
    int getSocketFD();
};

class STUNServerBundle {
  private:
    static const int MAX_EVENTS = 100;
    
    STUNSocket *primaryAddressPrimaryPortSocket;
    STUNSocket *primaryAddressSecondaryPortSocket;
    STUNSocket *secondaryAddressPrimaryPortSocket;
    STUNSocket *secondaryAddressSecondaryPortSocket;
    
  public:
    STUNServerBundle(STUNConfiguration &configuration);
    virtual ~STUNServerBundle();
    
    void start();
    void startAndCatchExceptions();
};