#include "socketAddress.h"

SocketAddress::SocketAddress(string ipAddrStr, string portStr) {
      unsigned short port = short(stoi(portStr));
      this->socket.sin_family = AF_INET;
      this->socket.sin_port = htons(port);
      this->socket.sin_addr.s_addr = 0;
      inet_aton(ipAddrStr.c_str(), &(this->socket.sin_addr));
}

SocketAddress::SocketAddress(sockaddr_in socket) {
      this->socket = socket;
}

struct sockaddr_in SocketAddress::getSocketAddrStruct() {
      return this->socket;
}

string SocketAddress::getIpAddress() {
    return string(inet_ntoa(this->socket.sin_addr));
}

string SocketAddress::getPort() {
    return to_string(ntohs(this->socket.sin_port));
}

string SocketAddressSerializer::Serialize(SocketAddress toSerialize) {
    string host = toSerialize.getIpAddress();
    string port = toSerialize.getPort();
    return host + ":" + port;
}

SocketAddress SocketAddressSerializer::Deserialize(string serializedAddress) {
    size_t colon = serializedAddress.find(":");
    if(colon == string::npos) {
	    throw string("Port not specified!\n");
    }
    string host = serializedAddress.substr(0, colon);
    string port = serializedAddress.substr(colon+1, serializedAddress.size()-colon);

    return SocketAddress(host, port);
}




