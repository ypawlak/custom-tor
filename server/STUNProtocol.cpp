#include "STUN.h"

Message STUNProtocol::createNUTSMessage(SocketAddress address) {
  string content = NUTS_PREFIX + "\n";
  content += PORT_PREFIX + address.getPort() + "\n";
  content += ADDRESS_PREFIX + address.getIpAddress() + "\n";
  return Message(address, content);
}

Message STUNProtocol::createSTUNMessage(SocketAddress address, bool changePort, bool changeAddress) {
  string content = STUN_PREFIX + "\n";
  content += CHANGE_PORT_PREFIX + ( changePort ? "TRUE" : "FALSE" ) + "\n";
  content += CHANGE_ADDRESS_PREFIX + ( changeAddress ? "TRUE" : "FALSE" ) + "\n";
  return Message(address, content);
}

STUNPacket STUNProtocol::readSTUNMessage(Message message) {
  string &content = message.MessageText;
  
  int stunPrefixPosition = content.find(STUN_PREFIX);
  int changePortPrefixPosition = content.find(CHANGE_PORT_PREFIX);
  int changeAddressPrefixPosition = content.find(CHANGE_ADDRESS_PREFIX);
  
  if(stunPrefixPosition == string::npos) {
    throw string("Malformed STUN packet! Missing " + STUN_PREFIX + " sequence\n");
  } else if(changePortPrefixPosition == string::npos) {
    throw string("Malformed STUN packet! Missing " + CHANGE_PORT_PREFIX + " sequence\n");
  } else if(changeAddressPrefixPosition == string::npos) {
    throw string("Malformed STUN packet! Missing " + CHANGE_ADDRESS_PREFIX + " sequence\n");
  }
  
  bool changePort = (content.substr(changePortPrefixPosition + CHANGE_PORT_PREFIX.size(), 4) == "TRUE");
  bool changeAddress = (content.substr(changeAddressPrefixPosition + CHANGE_ADDRESS_PREFIX.size(), 4) == "TRUE");
  
  return STUNPacket(message.Address, changePort, changeAddress);
}

SocketAddress STUNProtocol::readNUTSMessage(Message message) {
  string &content = message.MessageText;
  
  int nutsPrefixPosition = content.find(NUTS_PREFIX);
  int portPrefixPosition = content.find(PORT_PREFIX);
  int addressPrefixPosition = content.find(ADDRESS_PREFIX);
  
  if(nutsPrefixPosition == string::npos) {
    throw string("Malformed NUTS packet! Missing " + NUTS_PREFIX + " sequence\n");
  } else if(portPrefixPosition == string::npos) {
    throw string("Malformed NUTS packet! Missing " + PORT_PREFIX + " sequence\n");
  } else if(addressPrefixPosition == string::npos) {
    throw string("Malformed NUTS packet! Missing " + ADDRESS_PREFIX + " sequence\n");
  }
  
  int endlineAfterPortPosition = content.find("\n", portPrefixPosition);
  int endlineAfterAddressPosition = content.find("\n", addressPrefixPosition);
  
  if(endlineAfterPortPosition == string::npos) {
    throw string("Malformed NUTS packet! Missing newline after " + PORT_PREFIX + " sequence\n");
  } else if(endlineAfterAddressPosition == string::npos) {
    throw string("Malformed NUTS packet! Missing newline after " + ADDRESS_PREFIX + " sequence\n");
  }
  
  string port = content.substr(portPrefixPosition + PORT_PREFIX.size(), endlineAfterPortPosition-(portPrefixPosition + PORT_PREFIX.size()));
  string address = content.substr(addressPrefixPosition + ADDRESS_PREFIX.size(), endlineAfterAddressPosition-(addressPrefixPosition + ADDRESS_PREFIX.size()));
  
  return SocketAddress(address, port);
}