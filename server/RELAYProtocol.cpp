#include "RELAY.h"

Message RELAYProtocol::createHEREMessage(SocketAddress targetAddress, string name) {
  string content = HERE_PREFIX + "\n";
  content += name + "\n";
  return Message(targetAddress, content);
}

Message RELAYProtocol::createWHEREMessage(SocketAddress targetAddress, string name) {
  string content = WHERE_PREFIX + "\n";
  content += name + "\n";
  return Message(targetAddress, content);
}

Message RELAYProtocol::createTHEREMessage(SocketAddress targetAddress, RELAYEntity subject) {
  string content = THERE_PREFIX + "\n";
  content += subject.Name + "\n";
  content += SocketAddressSerializer::Serialize(subject.Address) + "\n";
  return Message(targetAddress, content);
}

Message RELAYProtocol::createUNKNOWNMessage(SocketAddress targetAddress, string name) {
  string content = UNKNOWN_PREFIX + "\n";
  content += name + "\n";
  return Message(targetAddress, content);
}

string RELAYProtocol::readHEREMessage(Message message) {
  size_t herePosition = message.MessageText.find(HERE_PREFIX);
  size_t newLinePosition = message.MessageText.find('\n');
  
  if(herePosition == string::npos || newLinePosition == string::npos) {
    throw string("Error: Malformed HERE message\n");
  }
  
  size_t secondNewLinePosition = message.MessageText.find('\n', newLinePosition+1);
  
  if(secondNewLinePosition == string::npos) {
    throw string("Error: Malformed HERE message\n");
  }
  
  return message.MessageText.substr(newLinePosition+1, secondNewLinePosition - newLinePosition - 1);
}

string RELAYProtocol::readWHEREMessage(Message message) {
  size_t wherePosition = message.MessageText.find(WHERE_PREFIX);
  size_t newLinePosition = message.MessageText.find('\n');
  
  if(wherePosition == string::npos || newLinePosition == string::npos) {
    throw string("Error: Malformed WHERE message\n");
  }
  
  size_t secondNewLinePosition = message.MessageText.find('\n', newLinePosition+1);
  
  if(secondNewLinePosition == string::npos) {
    throw string("Error: Malformed WHERE message\n");
  }
  
  return message.MessageText.substr(newLinePosition+1, secondNewLinePosition - newLinePosition - 1);
}

string RELAYProtocol::readUNKNOWNMessage(Message message) {
  size_t wherePosition = message.MessageText.find(UNKNOWN_PREFIX);
  size_t newLinePosition = message.MessageText.find('\n');
  
  if(wherePosition == string::npos || newLinePosition == string::npos) {
    throw string("Error: Malformed UNKNOWN message\n");
  }
  
  size_t secondNewLinePosition = message.MessageText.find('\n', newLinePosition+1);
  
  if(secondNewLinePosition == string::npos) {
    throw string("Error: Malformed UNKNOWN message\n");
  }
  
  return message.MessageText.substr(newLinePosition+1, secondNewLinePosition - newLinePosition - 1);
}

RELAYEntity RELAYProtocol::readTHEREMessage(Message message) {
  size_t therePosition = message.MessageText.find(THERE_PREFIX);
  size_t newLinePosition = message.MessageText.find('\n');
  
  if(therePosition == string::npos || newLinePosition == string::npos) {
    throw string("Error: Malformed THERE message\n");
  }
  
  size_t secondNewLinePosition = message.MessageText.find('\n', newLinePosition+1);
  
  if(secondNewLinePosition == string::npos) {
    throw string("Error: Malformed THERE message\n");
  }
  
  size_t thirdNewLinePosition = message.MessageText.find('\n', secondNewLinePosition+1);
  
  if(thirdNewLinePosition == string::npos) {
    throw string("Error: Malformed THERE message\n");
  }
  
  string name = message.MessageText.substr(newLinePosition+1, secondNewLinePosition - newLinePosition - 1);
  string address = message.MessageText.substr(secondNewLinePosition+1, thirdNewLinePosition-secondNewLinePosition-1);
  
  return RELAYEntity(SocketAddressSerializer::Deserialize(address), name);
}

RELAYPacketType RELAYProtocol::checkType(Message message) {
  if(message.MessageText.substr(0, HERE_PREFIX.size()) == HERE_PREFIX) {
    return RELAYPacketType::HERE;
  } else if(message.MessageText.substr(0, WHERE_PREFIX.size()) == WHERE_PREFIX) {
    return RELAYPacketType::WHERE;
  } else if(message.MessageText.substr(0, THERE_PREFIX.size()) == THERE_PREFIX) {
    return RELAYPacketType::THERE;
  } else if(message.MessageText.substr(0, UNKNOWN_PREFIX.size()) == UNKNOWN_PREFIX) {
    return RELAYPacketType::UNKNOWN;
  } else {
    return RELAYPacketType::MALFORMED;
  }
}
