#include "utilities.h"
#include "packetListener.h"

int main(int argc, char* argv[]) {
	
	string name = (argc > 1) ? argv[1] : "SERWERUS";
	unsigned short port = (argc > 2) ? (unsigned short)stoi(argv[2]) : 12345;
	string host = (argc > 3) ? argv[3] : "";
	
	try{
		UdpServer server(port, 1000, host);
		ServerPacketListener *r = new ServerPacketListener(&server, name);
		server.setListener(r);
		
		while(1) {
			try {
				server.receive();
			} catch(string s) {
				cout << "Error: " << s << endl;
			}
		}
	} catch(string s) {
		cout << "ERROR: " << s << endl;
	}
	return 0;
}
