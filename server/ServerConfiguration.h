#pragma prama once

#include "STUN.h"
#include <getopt.h>
#include <fstream>
#include <sstream>

#define REQUIRED_ARGUMENT 1
#define NO_ARGUMENT 0

class ServerConfiguration {
  public:
    string primaryAddress;
    string secondaryAddress;
    string relayAddress;
    
    bool stunOnlySet;
    bool relayOnlySet;
};

class CMDArguments {
  private:
    static ServerConfiguration readFromFile(string fileName);
  public:  
    static ServerConfiguration createConfigurationUsingCMDArguments(int argc, char *argv[]);
};