#pragma once

#include "utilities.h"

class ClientPacketListener : public UdpPacketListener {
	private:
		UdpServer *server; 
		map<string, SocketAddress> lookup;
		string myName;
	
		void handleHello(Message m);
		void handleCall(Message m);
		void handleCalled(Message m);
		void handleTold(Message m);
		void handleUnknown(Message m);
		
		void sendHello(Hello hello);
		void sendAck(Hello hello);
		void sendCalled(Called called);
	public:
		ClientPacketListener(UdpServer *_server, string _myName) : server(_server), myName(_myName) {}
		virtual void handlePacket(Message message);
};
