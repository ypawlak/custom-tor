#include "utilities.h"
#include "packetListener.h"

void ServerPacketListener::handlePacket(Message m) {
	switch(checkMessageType(m)) {
		
		case MessageType::HELLO:
			handleHello(m);
			break;
			
		case MessageType::TELL:
			handleTell(m);
			break;
			
		case MessageType::CALLED:
			handleCalled(m);
			break;
			
		default:
			cout << "Unrecognized message: " << m.MessageText << endl;
			;
	}
}

void ServerPacketListener::handleHello(Message m) {
	Hello hello(m);
	cout << "HELLO: " << hello.OriginName << "." << endl;
	if(lookup.find(hello.OriginName) == lookup.end()) {
		sendHello(hello);
	} else {
		sendAck(hello);
	}
	lookup[hello.OriginName] = hello.Source;
}

void ServerPacketListener::handleTell(Message m) {
	Tell tell(m);
	cout << "TELL: " << tell.TargetName << "." << endl;
	if(lookup.find(tell.TargetName) == lookup.end()) {
		sendUnknown(tell);
	} else {
		sendCall(tell);
		sendTold(tell);
	}
}

void ServerPacketListener::handleCalled(Message m) {
	cout << m.MessageText << endl;
	Called called(m);
	sendCalled(called);
}

void ServerPacketListener::sendHello(Hello hello) {
	hello.OriginName = myName;
	hello.Destination = hello.Source;
	server->send(hello.createMessage());
}

void ServerPacketListener::sendAck(Hello hello) {
	Message message(SocketAddress(), hello.Source, "ACK");
	server->send(message);
}

void ServerPacketListener::sendTold(Tell tell) {
	Message message(SocketAddress(), tell.Source, "TOLD");
	server->send(message);
}

void ServerPacketListener::sendCall(Tell tell) {
	Call call;
	call.Destination = lookup[tell.TargetName];
	call.TargetName = tell.OriginName;
	call.CallAddress = tell.Source;
	server->send(call.createMessage());
}

void ServerPacketListener::sendCalled(Called called) {
	called.Destination = called.CallAddress;
	called.CallerAddress = called.Source;
	server->send(called.createMessage());
}

void ServerPacketListener::sendUnknown(Tell tell) {
	Message message(SocketAddress(), tell.Source, "UNKNOWN");
	server->send(message);
}
