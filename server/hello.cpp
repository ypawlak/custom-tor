#include "hello.h"

Hello::Hello(Message msg) {
    this->Address = SocketAddress(msg.Address.getSocketAddrStruct());
    this->OriginName = msg.MessageText.substr(string(HELLO_PREFIX).length());
    if(OriginName.find("\n") != string::npos) {
		OriginName = OriginName.substr(0, OriginName.find("\n"));
	}
}

Message Hello::createMessage() {
    string txt = HELLO_PREFIX + this->OriginName;
    return Message(this->Address, txt);
}
