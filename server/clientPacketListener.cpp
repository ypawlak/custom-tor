#include "utilities.h"
#include "clientPacketListener.h"

void ClientPacketListener::handlePacket(Message m) {
	switch(checkMessageType(m)) {
		
		case MessageType::HELLO:
			handleHello(m);
			break;
			
		case MessageType::TOLD:
			handleTold(m);
			break;
			
		case MessageType::CALL:
			handleCall(m);
			break;
			
		case MessageType::CALLED:
			handleCalled(m);
			break;
			
		case MessageType::UNKNOWN:
			handleUnknown(m);
			break;
			
		default:
			;
	}
}

void ClientPacketListener::handleHello(Message m) {
	Hello hello(m);
	cout << "HELLO: " << hello.OriginName << "." << endl;
	if(lookup.find(hello.OriginName) == lookup.end()) {
		cout << "SEND HELLO TO " << SocketAddressSerializer::Serialize(hello.Source) << endl;
		Hello response;
		response.OriginName = myName;
		response.Destination = hello.Source;
		sendHello(response);
	} else {
		sendAck(hello);
	}
	lookup[hello.OriginName] = hello.Source;
}

void ClientPacketListener::handleCall(Message m) {
	Call call(m);
	cout << "CALL: " << call.TargetName << "." << endl;
	cout << m.MessageText << endl;
	cout << SocketAddressSerializer::Serialize(call.Destination) << endl;
	Hello hello;
	hello.OriginName = myName;
	hello.Destination = call.CallAddress;
	sendHello(hello);
	Called called;
	called.CallerAddress = call.Destination;
	called.CallAddress = call.CallAddress;
	called.Destination = call.Source;
	called.OriginName = myName;
	called.TargetName = call.TargetName;
	sendCalled(called);
}

void ClientPacketListener::handleCalled(Message m) {
	Called called(m);
	cout << m.MessageText << endl;
}

void ClientPacketListener::handleUnknown(Message m) {
	cout << m.MessageText << endl;
}

void ClientPacketListener::handleTold(Message m) {
	cout << m.MessageText << endl;
}

void ClientPacketListener::sendHello(Hello hello) {
	server->send(hello.createMessage());
}

void ClientPacketListener::sendAck(Hello hello) {
	cout << "SEND ACK" << endl;
	Message message(SocketAddress(), hello.Source, "ACK");
	server->send(message);
}

void ClientPacketListener::sendCalled(Called called) {
	cout << "SEND CALLED" << endl;
	server->send(called.createMessage());
}
