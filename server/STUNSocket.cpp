#include "STUN.h"

STUNSocket::STUNSocket(SocketAddress &address) {
    
  socketFD = socket(AF_INET, SOCK_DGRAM, 0);

  if(socketFD == -1) {
    throw string("Error creating socket\n");
  }
  
  struct sockaddr_in addr = address.getSocketAddrStruct();
    
  int errorCode = bind(socketFD, (const sockaddr*) &(addr), sizeof(addr));
    
  if (errorCode < 0) {
    throw string("Error binding socket to " + address.getIpAddress() + ":" + address.getPort() + "\n");
  }
  
  int flags = fcntl(socketFD,F_GETFL,0);
  
  if (flags < 0) {
    throw string("Error making socket at " + address.getIpAddress() + ":" + address.getPort() + " nonblocking\n");
  }
  
  errorCode = fcntl(socketFD, F_SETFL, flags | O_NONBLOCK);
  
  if (flags < 0) {
    throw string("Error making socket at " + address.getIpAddress() + ":" + address.getPort() + " nonblocking\n");
  }
  
  buffer = new char[bufferSize];
}

STUNSocket::~STUNSocket() {
  close(socketFD);
  delete buffer;
}

void STUNSocket::sendMessage(Message &message) {
  int toSend = message.MessageText.size();
  sockaddr_in address = message.Address.getSocketAddrStruct();
  while(toSend > 0) {
    int sent = sendto(socketFD, message.MessageText.c_str()+(message.MessageText.size())-toSend, toSend, 0, (const sockaddr*) &address, sizeof(sockaddr));
    if(sent < 0) {
      throw string("Error sending data!\n");
    } else {
      toSend -= sent;
    }
  }
}

void STUNSocket::handleSTUNPacket(STUNPacket &packet) {
  Message nutsMessage = STUNProtocol::createNUTSMessage(packet.Address);
  if(packet.ChangePort == false) {
    if(packet.ChangeAddress == false) {
      sendMessage(nutsMessage);
    } else {
      samePortDifferentAddressSocket->sendMessage(nutsMessage);
    }
  } else {
    if(packet.ChangeAddress == false) {
      differentPortSameAddressSocket->sendMessage(nutsMessage);
    } else {
      differentPortDifferentAddressSocket->sendMessage(nutsMessage);
    }
  }
}

void STUNSocket::setNeighbourhood(STUNSocket *_samePortDifferentAddressSocket, STUNSocket *_differentPortSameAddressSocket, STUNSocket *_differentPortDifferentAddressSocket) {
  samePortDifferentAddressSocket = _samePortDifferentAddressSocket;
  differentPortSameAddressSocket = _differentPortSameAddressSocket;
  differentPortDifferentAddressSocket = _differentPortDifferentAddressSocket;
}
    
void STUNSocket::receive() {
  sockaddr_in sourceAddr;
  unsigned int addrLen = sizeof(sourceAddr);
  
  int totalRead = recvfrom(socketFD,
      buffer,
      bufferSize,
      0,
      (sockaddr*) &sourceAddr,
      &addrLen);
  
  if (totalRead < 0) {
    throw string("Error in receive function\n");
  }
  
  Message message(SocketAddress(sourceAddr), string(buffer, totalRead));
  
  STUNPacket packet = STUNProtocol::readSTUNMessage(message);
  
  handleSTUNPacket(packet);
}

int STUNSocket::getSocketFD() {
  return socketFD;
}