#pragma once

#include "message.h"
#include "socketAddress.h"

static const string HERE_PREFIX = "HERE";
static const string WHERE_PREFIX = "WHERE";
static const string THERE_PREFIX = "THERE";
static const string UNKNOWN_PREFIX = "UNKNOWN";

enum class RELAYPacketType { HERE, WHERE, THERE, UNKNOWN, MALFORMED };

class RELAYEntity {
  public:
    RELAYEntity(SocketAddress _address, string _name) : Address(_address), Name(_name) { }
    SocketAddress Address;
    string Name;
};

class RELAYProtocol {
  private:
  public:
    static Message createHEREMessage(SocketAddress targetAddress, string name);
    static Message createWHEREMessage(SocketAddress targetAddress, string name);
    static Message createTHEREMessage(SocketAddress targetAddress, RELAYEntity subject);
    static Message createUNKNOWNMessage(SocketAddress targetAddress, string name);
    static string readHEREMessage(Message message);
    static string readWHEREMessage(Message message);
    static string readUNKNOWNMessage(Message message);
    static RELAYEntity readTHEREMessage(Message message);
    static RELAYPacketType checkType(Message message);
};

class RELAYSocket {
  private:
    static const int bufferSize = 1024;
    
    int socketFD;
    char *buffer;
    
    map<string, SocketAddress> lookup;
    
    void sendMessage(Message &message);
    
  public:
    RELAYSocket(SocketAddress &address);
    virtual ~RELAYSocket();
    
    void receive();
    void startAndCatchExceptions();
};