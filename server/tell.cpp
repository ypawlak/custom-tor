#include "tell.h"

Tell::Tell(Message msg) {
    this->Address = SocketAddress(msg.Address.getSocketAddrStruct());
    int atPos = msg.MessageText.find('@');
    int start = string(TELL_PREFIX).length();
    this->TargetName = msg.MessageText.substr(start, atPos -start);
    this->OriginName = msg.MessageText.substr(atPos + 1);
    if(OriginName.find("\n") != string::npos) {
		OriginName = OriginName.substr(0, OriginName.find("\n"));
	}
    if(TargetName.find("\n") != string::npos) {
		TargetName = TargetName.substr(0, TargetName.find("\n"));
	}
}

Message Tell::createMessage() {
    string txt = TELL_PREFIX + this->TargetName + "@" + this->OriginName;
    return Message(this->Address, txt);
}
