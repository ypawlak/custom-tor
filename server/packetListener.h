#pragma once

#include "utilities.h"

class ServerPacketListener : public UdpPacketListener {
	private:
		UdpServer *server; 
		map<string, SocketAddress> lookup;
		string myName;
	
		void handleHello(Message m);
		void handleTell(Message m);
		void handleCalled(Message m);
		
		void sendHello(Hello hello);
		void sendAck(Hello hello);
		void sendTold(Tell tell);
		void sendCall(Tell tell);
		void sendCalled(Called called);
		void sendUnknown(Tell tell);
	public:
		ServerPacketListener(UdpServer *_server, string _myName) : server(_server), myName(_myName) {}
		virtual void handlePacket(Message m);
};
