#include "called.h"

Called::Called(Message msg) {
    this->Address = SocketAddress(msg.Address.getSocketAddrStruct());
    int atPos = msg.MessageText.find('@');
    int fromPos = msg.MessageText.find(":FROM:");
    int secondAtPos = msg.MessageText.find('@', fromPos);
    int start = string(CALLED_PREFIX).length();
    this->TargetName = msg.MessageText.substr(start, atPos-start);
    this->CallAddress = SocketAddressSerializer::Deserialize(msg.MessageText.substr(atPos + 1, fromPos-atPos-1));
    this->OriginName = msg.MessageText.substr(fromPos+6, secondAtPos-fromPos-6);
    this->CallerAddress = SocketAddressSerializer::Deserialize(msg.MessageText.substr(secondAtPos + 1));
}

Message Called::createMessage() {
    string txt = CALLED_PREFIX + this->TargetName + "@" + SocketAddressSerializer::Serialize(this->CallAddress)
		+ ":FROM:" + this->OriginName + "@" + SocketAddressSerializer::Serialize(this->CallerAddress);
    return Message(this->Address, txt);
}
