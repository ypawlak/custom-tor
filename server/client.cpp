#include "utilities.h"
#include "clientPacketListener.h"

int main(int argc, char* argv[]) {
	
	string name = (argc > 1) ? argv[1] : "KLIENT";
	unsigned short port = (argc > 2) ? (unsigned short) stoi(argv[2]) : 12345;
	string interface = (argc > 3) ? argv[3] : "";
	string remoteHost = (argc > 4) ? argv[4] : "127.0.0.1";
	string remotePort = (argc > 5) ? argv[5] : "12345";
	
	try{
		UdpServer server(port, 1000, interface);
		ClientPacketListener *r = new ClientPacketListener(&server, name);
		server.setListener(r);
		
		Hello hello;
		hello.Destination = SocketAddress(remoteHost, remotePort);
		hello.OriginName = name;
		server.send(hello.createMessage());
		cout << hello.createMessage().MessageText << " " << SocketAddressSerializer::Serialize(hello.Destination) << endl;
		
		while(1) {
			try {
				server.receive();
			} catch(string s) {
				cout << "Error: " << s << endl;
			}
		}
	} catch(string s) {
		cout << "ERROR: " << s << endl;
	}
	return 0;
}
