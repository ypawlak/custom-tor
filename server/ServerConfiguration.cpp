#include "ServerConfiguration.h"


ServerConfiguration CMDArguments::readFromFile(string fileName) {
  ifstream infile(fileName);
  
  ServerConfiguration configuration;
  
  string line, prefix, value;;
  while (getline(infile, line))
  {
    istringstream iss(line);
    if (!(iss >> prefix >> value)) { continue; }

    if(prefix == "PRIMARY:") configuration.primaryAddress = value;
    else if(prefix == "SECONDARY:") configuration.secondaryAddress = value;
    else if(prefix == "RELAY:") configuration.relayAddress = value;
  }
  
  return configuration;
}

ServerConfiguration CMDArguments::createConfigurationUsingCMDArguments(int argc, char* argv[]) {
  
  ServerConfiguration configuration;
  
  string configurationFile;
  bool configurationFileSet;
  bool primaryAddressSet = false;
  bool secondaryAddressSet = false;
  bool relayAddressSet = false;  
  
  configurationFileSet = primaryAddressSet = secondaryAddressSet = relayAddressSet = configuration.stunOnlySet = configuration.relayOnlySet = false;
  int c = 0, option_index = 0;
  
  int stunOnlySet = 0, relayOnlySet = 0;
  
  static struct option long_options[] = {
    {"configuration", REQUIRED_ARGUMENT, 0, 'c'},
    {"primary", REQUIRED_ARGUMENT, 0, 'p'},
    {"secondary", REQUIRED_ARGUMENT, 0, 's'},
    {"relay", REQUIRED_ARGUMENT, 0, 'r'},
    {"stunonly", NO_ARGUMENT, &(stunOnlySet), 1},
    {"relayonly", NO_ARGUMENT, &(relayOnlySet),  1},
    {0, 0, 0, 0}
  };
  
  while (c = getopt_long(argc, argv, "c:p:s:r:", long_options, &option_index), c != -1) {
    switch (c) {

      case 'c':
	configurationFileSet = true;
	configurationFile = optarg;
	break;

      case 'p':
	primaryAddressSet = true;
	configuration.primaryAddress = optarg;
	break;

      case 's':
	secondaryAddressSet = true;
	configuration.secondaryAddress = optarg;
	break;
	  
      case 'r':
	relayAddressSet = true;
	configuration.relayAddress = optarg;
	break;

      case '?':
	break;

      default:
	break;
    }
  }
  
  if(stunOnlySet) configuration.stunOnlySet = true;
  if(relayOnlySet) configuration.relayOnlySet = true;
  
  if(configurationFileSet) {
    ServerConfiguration configurationFromFile = readFromFile(configurationFile);
    if(!primaryAddressSet) configuration.primaryAddress = configurationFromFile.primaryAddress;
    if(!secondaryAddressSet) configuration.secondaryAddress = configurationFromFile.secondaryAddress;
    if(!relayAddressSet) configuration.relayAddress = configurationFromFile.relayAddress;
  }
  
  if(configuration.stunOnlySet && configuration.relayOnlySet) {
    throw string("Error: cannot create neither STUN nor RELAY instance!\n");
  } else if(!configuration.relayOnlySet && (configuration.primaryAddress.size() == 0 || configuration.secondaryAddress.size() == 0)) {
    throw string("Error: cannot create STUN instance (missing PRIMARY or SECONDARY address) !\n");
  } else if(!configuration.stunOnlySet && (configuration.relayAddress.size() == 0)) {
    throw string("Error: cannot create RELAY instance (missing RELAY address) !\n");
  }
  
  return configuration;
}