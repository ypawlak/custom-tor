#pragma once
#include "message.h"

#define TELL_PREFIX "TELL:"

class Tell {
    public:
      Tell() {}
      Tell(Message msg);
      SocketAddress Address;
      string OriginName;
      string TargetName;
      Message createMessage();
};

