#pragma once
#include "message.h"

#define HELLO_PREFIX "HELLO:"

class Hello {
    public:
      Hello() {}
      Hello(Message msg);
      SocketAddress Address;
      string OriginName;
      Message createMessage();
};

